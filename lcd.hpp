#pragma once
#include <string>

#include "Library/wiringPi/wiringPi.h"
#include "Library/wiringPi/wiringPiI2C.h"

class lcd
{
	
public:
	enum LCD_Command {
		 LCD_CHR = 1 // Mode - Sending data
		 ,LCD_CMD = 0 // Mode - Sending command
		 ,LINE1=  0x80 // 1st line
		 ,LINE2=  0xC0 // 2nd line
		 ,LCD_BACKLIGHT  = 0x08  // On LCD_BACKLIGHT = 0x00  # Off
		 ,LCD_MAXLEN = 16
		 ,ENABLE = 0b00000100 // Enable bit
	};
	int addr_;
	std::string i2c;
	lcd() = default;
	lcd(std::string i2c, int nAddr);
	auto lcd_byte(uint8_t byte)->void;
private:
	auto lcd_init()->void;
	int file_descriptor_;
};

inline lcd::lcd(std::string i2c, int nAddr)
{
	file_descriptor_ = wiringPiI2CSetupInterface(i2c.c_str(), nAddr);
}

inline auto lcd::lcd_byte(uint8_t byte) -> void
{
}

inline auto lcd::lcd_init() -> void
{
	// Initialise display
	this->lcd_byte(0x33, LCD_CMD);   // Initialise
	this->lcd_byte(0x32, LCD_CMD);    // Initialise
	this->lcd_byte(0x06, LCD_CMD);    // Cursor move direction
	this->lcd_byte(0x0C, LCD_CMD);    // 0x0F On, Blink Off
	this->lcd_byte(0x28, LCD_CMD);    // Data length, number of lines, font size
	this->lcd_byte(0x01, LCD_CMD);    // Clear display
	delayMicroseconds(500);
}
