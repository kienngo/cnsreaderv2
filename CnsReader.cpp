
#include <iostream>
//#include <CnsReader.h>

#include <configMachine.hpp>
#include <mutex>
#include <thread>
#include <ThreadManager.h>


#include "CnsExtend.h"
#include "gpioService.h"
#include "ultils.h"
#include "wiringPi.h"
#include "Library/lcd/lcdService.h"
#include "Library/timeService/timeService.h"
#include "Library/wiringPi/wiringPi.h"
#include "gpioService.h"
#include "K3Machine.h"
using namespace std;
static volatile int globalCounter[8];
extern uint8_t buzzer_en;
float personTemperature;
void timerHandler(int sig, siginfo_t* si, void* uc);
void AlertTimerHandler(int sig, siginfo_t* si, void* uc);

//LED_RED,
//		LED_GREEN,
//		LED_BLUE,
//		LED_RG,
//		LED_RB,
//		LED_GB,
//		LED_RGB,
std::map<std::string, gpioService::GPIO> ledMap { {"LED_RED", gpioService::LED_RED}, {"LED_GREEN", gpioService::LED_GREEN}, {"LED_BLUE", gpioService::LED_BLUE}, {"LED_YELLOW", gpioService::LED_RG}}
;
configMachine config;
lcdService* lcd;
gpioService* gpio;
timeService main_tick_timer = timeService(timerHandler);
timeService k3_timer = timeService(AlertTimerHandler);

mutex attendance_mutex;
mutex semi_mutex;
void timerHandler(int sig, siginfo_t* si, void* uc)
{

//	cout << "Config device------------------------------------------------ \n";
	//lcd->lcd_mainScreen();

}

/*
 * myInterrupt:
 *********************************************************************************
 */

void AlertTimerHandler(int sig, siginfo_t* si, void* uc)
{
	config.mK3_Config.k3_measure_status = TEMP_NOT_MEASURED;
	config.mK3_Config.k3_TempSafety = NO_WARNING;
			
}
void WorkEngine(CnsExtended cns_reader) {
	std::string cardNumber;
	std::string name_str;
	
	//
	//Start Engine
	//
	if(!cns_reader.StartReadCard()) return;
	
	//Read Card Success- Get Name - Get Card Number
	//
	cardNumber = cns_reader.CnsGetCardNumber();
	name_str = cns_reader.CnsGetName();


	cns_reader.PICC_HaltA();
	cns_reader.PCD_StopCrypto1();

	//validate name and card number
	if (name_str.empty() || cardNumber.empty())
	{
		//gpio->gpio_control(gpioService::LED_RED, gpioService::BLINKY, 300);
		//lcd->lcd_display(name_str, lcdService::TAG_INVALID, lcdService::STATIC_, 900);
		return;
	}
	
	//TODO: For Body temperature safety
	//
	//Check level
	if(config.device.bEnaTempSafety)
	{
		switch (config.device.nTempSafetyLevel)
		{
		case 0:
			//Normal mode
			//Warning mode
				//
			if (TEMP_NOT_MEASURED == config.mK3_Config.k3_measure_status)
			{
				//
				//Show screen remind measure temperature


				//Show remind screen
				buzzer_en = 1;
				lcd->lcd_display(name_str, lcdService::K3_REMIND, lcdService::STATIC_, config.mK3_Config.remind_timeout);

				return;
			}
			break;
		case 1:
			//Warning mode
			//
			{
				//Warning mode
				//
				if(TEMP_NOT_MEASURED == config.mK3_Config.k3_measure_status)
				{
					//
					//Show screen remind measure temperature

			
					//Show remind screen
					buzzer_en = 1;
					lcd->lcd_display(name_str, lcdService::K3_REMIND, lcdService::STATIC_, config.mK3_Config.remind_timeout);
			         
					return;
				}
				if (config.mK3_Config.k3_TempSafety == HIGH_SAFETY)
				{
					//Reset State
					config.mK3_Config.k3_measure_status = TEMP_NOT_MEASURED;
					config.mK3_Config.k3_TempSafety = NO_WARNING;
					//
					k3_timer.stop_timer();


					buzzer_en = 2;
					lcd->lcd_display(name_str, lcdService::K3_REJECT, lcdService::STATIC_, config.mK3_Config.reject_timeout);
					gpio->gpio_control(ledMap[config.device_msg_.msg_tag_failed.led.c_str()], gpioService::TURN_ON, config.mK3_Config.reject_timeout);
					//Show  screen
					return;
				}

				
				//Reset State
				config.mK3_Config.k3_measure_status = TEMP_NOT_MEASURED;
				config.mK3_Config.k3_TempSafety = NO_WARNING;
				k3_timer.stop_timer();
			}
			break;
		}	
	}
	
	//Check TemperatureFlag

	//
	//TODO: Checkin event whitelist
	//
	char timeFormat[80];
	struct tm* tm;
	get_time(&tm);
	char timeSearchEvent[50];
	sprintf(timeFormat, "%04d%02d%02d%02d%02d%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	
	//Format
	//yyyy-mm-dd hh:mm
	//
	sprintf(timeSearchEvent, "%02d:%02d", tm->tm_hour, tm->tm_min);
//	std::cout << "Check in event available in this time: " << timeSearchEvent << '\n';
	std::vector<eventInformation_t > vecValidEvent = FindEvent(std::string(timeSearchEvent));
	//
	//TODO: Write attendance logs
	//
//	std::cout << "Event available: " << std::endl;
	if (vecValidEvent.size() == 0) {
		cout << "Have not event in this time \n";
//		return;
	}
	for (auto& event : vecValidEvent)
		std::cout << event.eventId << std::endl;
	
	//
	//Check config.bEnaAccessControl
	// if true - > check whitelist
	// false - > dont check whitelist -> write attendace without semiboarding -> return
//	if (config.bEnaAccessControl == false) {
//		//
//		//Write log attendance without semi boarding
//		//
//		buzzer_en = 1;
//		cout << "Don't check white list \n";
//		write_log_attendance(cardNumber, timeFormat, false,"00.0");
//		
//		lcd->lcd_display(name_str, lcdService::TAG_WELCOME, lcdService::STATIC_, config.device_msg_.msg_default.timeout);
//		
//		
//		return;
//	}
	
	//
	//Check config.bEnaAccessControl
	//
	// true - > check whitelist -> write attendace with or without semiboarding -> return
	
	checkWhitelistCode_t whitelist_check = checkByEvent(vecValidEvent, cardNumber);
	
	
	bool flag_check = true;
	if(config.device.bEnaAccessControl)
	{
		flag_check = whitelist_check.valid;
	}
	
	if (flag_check)
	{

		buzzer_en = 1;

		
		bool SemiBoardingOrNot = false;
		if (config.device.bEnaSemiBoarding)
		{
			SemiBoardingOrNot = whitelist_check.accessControlOnly;
		}
			
		std::string TempPerson = std::to_string(personTemperature); 
		write_log_attendance(cardNumber, timeFormat, SemiBoardingOrNot, TempPerson.substr(0, 4));
		
		
		
		if (SemiBoardingOrNot)
		{
			gpio->gpio_control(ledMap[config.device_msg_.msg_tag_success.led.c_str()], gpioService::TURN_ON, config.device_msg_.msg_semi_success.timeout);
			lcd->lcd_display(name_str, lcdService::TAG_SEMIBOARDING, lcdService::STATIC_, config.device_msg_.msg_semi_success.timeout);

		}
		else {
			gpio->gpio_control(ledMap[config.device_msg_.msg_tag_success.led.c_str()], gpioService::TURN_ON, config.device_msg_.msg_default.timeout);
			lcd->lcd_display(name_str, lcdService::TAG_WELCOME, lcdService::STATIC_, config.device_msg_.msg_default.timeout);
			
		}
	}
	else
	{
		buzzer_en = 2;

		gpio->gpio_control(ledMap[config.device_msg_.msg_tag_failed.led.c_str()], gpioService::TURN_ON, config.device_msg_.msg_tag_failed.timeout);
		lcd->lcd_display(name_str, lcdService::TAG_REJECT, lcdService::STATIC_, config.device_msg_.msg_tag_failed.timeout);
	}
	name_str.clear();
	cardNumber.clear();
//	write_log_attendance(cardNumber, buf, whitelist_check);
}

#ifdef TEST_ENVIR
bool IsWordAligned(void* addr)
{
	return (((unsigned long)addr) & 0x3) == 0;
}

TEST(DemoTests, AllocationTest)
{
	char* p = new char[1024];
	ASSERT_TRUE(p != NULL);
	ASSERT_PRED1(IsWordAligned, p);
	delete[] p

	ASSERT_THROW(p = new char[2 * 1024 * 1024 * 1024], std::bad_alloc);
}
#endif
int main(int argc, char *argv[])
{	
	wiringPiSetup();
	
	
	cout << "Config device------------------------------------------------ \n";
	config  =  configMachine("product");
	
	CnsExtended cns_reader = CnsExtended();
	
	cout << "Start tick Timer-------------------------------------------------- \n";


	//Create K3 check timer 
	k3_timer.createTimer("K3MSG", 15,SIGUSR2);
	cout << "Start Engine------------------------------------------------  \n";
	
	ThreadManagedInit();

	
	
	uint16_t count = 0;
	char test[100];

#ifdef TEST_ENVIR
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
#endif

	while (true)
	{
		
		WorkEngine(cns_reader);
		msleep(200);


	}
}
