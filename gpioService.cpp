﻿#include "gpioService.h"
#include <chrono>

#include "Fault.h"
#include "InnerTimer.h"
#include "ultils.h"

using GpioThreadMsg = enum
{
	GPIO_POST_USER_DATA,
	GPIO_POST_INNER,
	GPIO_MSG_TIMER,
	GPIO_EXIT_THREAD
};
struct GpioData
{
	gpioService::GPIO led = gpioService::LED_RGB;
	gpioService::Led_State state;
	gpioService::BuzzerState buzzer_state;
	std::string msg;
	unsigned int timeout;
	uint8_t BlinkyTimes;
};
struct ThreadMsg
{
	ThreadMsg(int prior, GpioThreadMsg i, std::shared_ptr<void> m) { priority = prior; id = i; msg = std::move(m); }
	int priority;
	GpioThreadMsg id;
	std::shared_ptr<void> msg;
	bool operator<(const ThreadMsg& _threadMsg) const {
		return priority < _threadMsg.priority;
	}
};
gpioService::gpioService()
{
}

gpioService::gpioService(int red, int green, int blue, int buzzer, int buzzer_freq)
{
	this->led_pin[0]  = red;
	this->led_pin[1] = green;
	this->led_pin[2] = blue;
	this->buzzer_pin = buzzer;
	this->buzzer_freq = buzzer_freq;
	
		
	for (int i = 0; i < 3; i++)
	{
	
		pinMode(this->led_pin[i], OUTPUT);
		digitalWrite(led_pin[i], HIGH);
	}
	digitalWrite(led_pin[2], LOW);
	this->m_timerExit = true;
	this->CreateThread();
}

void gpioService::gpio_control(GPIO led, Led_State state, unsigned int timeout)
{
	std::shared_ptr<GpioData> GpioMsg(new GpioData());
	GpioMsg->led = led;
	GpioMsg->state = state;
	GpioMsg->timeout = timeout;
	GpioMsg->BlinkyTimes = timeout/200;


	this->post_gpio_msg(GpioMsg);
}
void gpioService::gpio_control_inner(GPIO led, Led_State state,  unsigned int timeout)
{
	std::shared_ptr<GpioData> GpioMsg(new GpioData());
	GpioMsg->led = led;
	GpioMsg->state = state;
	GpioMsg->timeout = timeout;
	GpioMsg->BlinkyTimes = 3;


	this->post_gpio_inner_msg(GpioMsg, timeout);
}
void gpioService::gpio_led_blinky(GPIO led, Led_State state, int timeout)
{

	int gpioAvail[3] = {};
	 
	//Check led use
	switch (led)
	{
	case LED_RED:
		gpioAvail[0] = 1;
		break;
	case LED_GREEN:
		gpioAvail[0] = 1;
		break;
	case LED_BLUE:
		gpioAvail[0] = 1;
		break;
	case LED_RG:
		gpioAvail[0] = 1;
		gpioAvail[1] = 1;
		break;
	case LED_RB:
		gpioAvail[0] = 1;
		gpioAvail[2] = 1;
		break;
	case LED_GB:
		gpioAvail[1] = 1;
		gpioAvail[2] = 1;
		break;
	case LED_RGB:
		gpioAvail[0] = 1;
		break;
	}

	int pinVal;
	//Make led is low
	for (int i = 0; i < 3; i++)
    {
		pinVal= digitalRead(gpioAvail[i] & this->led_pin[i]);
	    digitalWrite(gpioAvail[i]&this->led_pin[i], pinVal^0xFF);
	}

}

void gpioService::gpio_turn_on(int pinMask[])
{
	for (int i = 0; i < 3; i++)
	{
		int mask = pinMask[i];
		digitalWrite(this->led_pin[i], mask);
	}
	
}

void gpioService::gpio_turn_off(int pinMask[])
{
	for (int i = 0; i < 3; i++)
	{
		
		int mask = pinMask[i] * this->led_pin[i];
		if (mask != 0) digitalWrite(mask, LOW);
	}
}

void gpioService::gpio_toggle(int pinMask[])
{

	int mask = 0;
	int pinVal = 0;
	for (int i = 0; i < 3; i++)
	{
		mask = pinMask[i] * this->led_pin[i];
		if (mask == 0) {
			digitalWrite(pinMask[i], LOW);
			continue;
		}
		pinVal = digitalRead(mask);
		if (pinVal) {
			digitalWrite(mask, LOW);
		}
		else {
			digitalWrite(mask, HIGH);
		}
	}

	
	
}

gpioService::~gpioService()
{
	
}

void gpioService::ExitThread()
{
}


void gpioService::dispatch_process()
{
	std::cout << "gpio thread start \r";
	std::thread timerThread(&gpioService::TimerThread, this);

	while (true)
	{
		std::shared_ptr<ThreadMsg> thread_msg;
		{
			// Wait for a message to be added to the stack
			std::unique_lock<std::mutex> lk(m_mutex);
			while (m_queue.empty())
				m_cv.wait(lk);

			if (m_queue.empty())
				continue;

			thread_msg = std::move(m_queue.front());
			m_queue.pop();
		}

		switch (thread_msg->id)
		{
		case GPIO_POST_USER_DATA:
		{
			this->m_timerExit = true;
			gpio_process_msg(thread_msg);
			break;
		}
		case GPIO_MSG_TIMER:
			
			this->m_timerExit = false;
			break;

		case GPIO_EXIT_THREAD:
		{
			this->m_timerExit = true;
			timerThread.join();
			break;
		}
		case GPIO_POST_INNER: 
			break;
		default: ;
		}
	}
}

void gpioService::TimerThread()
{
	while (true)
	{
		if (this->m_timerExit == false)
		{
			if (this->blinkyTimes-- != 0)
			{
				if (this->blinkyTimes == 0)
				{
					this->m_timerExit = true;
					std::cout << "gpio blinky off\r";
					this->gpio_control(LED_RGB, TURN_ON, 100);

				}
				else
				{
					gpio_toggle(this->gpio_affected_);
				}
			}
			
		}
		msleep(200);
	}
}
void gpioService::post_gpio_timer_msg(std::shared_ptr<GpioData> msg)
{
	ASSERT_TRUE(m_thread);

	// Create a new ThreadMsg
	std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(1, GPIO_MSG_TIMER, msg));

	// Add user data msg to queue and notify worker thread
	std::cout << "gpio post to queue \r";
	std::unique_lock<std::mutex> lk(m_mutex);
	m_queue.push(threadMsg);
	m_cv.notify_one();
}
void gpioService::post_gpio_msg(std::shared_ptr<GpioData> msg)
{
	ASSERT_TRUE(m_thread);

	// Create a new ThreadMsg
	std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(1, GPIO_POST_USER_DATA, msg));

	// Add user data msg to queue and notify worker thread
	std::cout << "gpio post to queue \r";
	std::unique_lock<std::mutex> lk(m_mutex);
	m_queue.push(threadMsg);
	m_cv.notify_one();
}

void gpioService::post_gpio_inner_msg(std::shared_ptr<GpioData> msg, int timeout)
{
	ASSERT_TRUE(m_thread);

	// Create a new ThreadMsg
	std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(2, GPIO_POST_USER_DATA, msg));

	auto NotifyFunc = [&]()
	{
		// Add user data msg to queue and notify worker thread
		std::cout << "gpio post to queue \r";
		std::unique_lock<std::mutex> lk(m_mutex);
		m_queue.push(threadMsg);
		m_cv.notify_one();

	};
	InnerTimer notify_inner_timer{timeout, NotifyFunc };
}

void gpioService::gpio_process_msg(std::shared_ptr<ThreadMsg> thread_msg)
{
	if (thread_msg == nullptr) return;

	const auto user_data = std::static_pointer_cast<GpioData>(thread_msg->msg);

	//std::cout << "Data process: " << user_data->state << "\r" << "Data msg: " << user_data->msg << std::endl;

	int gpio_affected[3];

	//Check led use
	switch (user_data->led)
	{
	case LED_RED:
		gpio_affected[0] = 1;
		gpio_affected[1] = 0;
		gpio_affected[2] = 0;
		break;
	case LED_GREEN:
		gpio_affected[0] = 0;
		gpio_affected[1] = 1;
		gpio_affected[2] = 0;
		break;
	case LED_BLUE:
		gpio_affected[0] = 0;
		gpio_affected[1] = 0;
		gpio_affected[2] = 1;
		break;
	case LED_RG:
		gpio_affected[0] = 1;
		gpio_affected[1] = 1;
		gpio_affected[2] = 0;
		break;
	case LED_RB:
		gpio_affected[0] = 1;
		gpio_affected[1] = 0;
		gpio_affected[2] = 1;
		break;
	case LED_GB:
		gpio_affected[0] = 0;
		gpio_affected[1] = 1;
		gpio_affected[2] = 1;
		break;
	case LED_RGB:
		gpio_affected[0] = 1;
		gpio_affected[1] = 1;
		gpio_affected[2] = 1;
		break;
	}

	
	
	switch (user_data->state)
	{
	case NONE_EFFECT:
		gpio_turn_on(gpio_affected);
		break;
	case BLINKY:
		gpio_toggle(gpio_affected);
		{
			
			std::copy(std::begin(gpio_affected), std::end(gpio_affected), std::begin(this->gpio_affected_));
		    this->blinkyTimes = user_data->BlinkyTimes;
			this->post_gpio_timer_msg(user_data);
		}	
		break;
	case TOGGLE:
		gpio_toggle(gpio_affected);
		break;
	case TURN_OFF:
		gpio_turn_off(gpio_affected);
		break;
	case TURN_ON:
		gpio_turn_on(gpio_affected);
		auto defaultFunc = [&]()
		{
			user_data->led = LED_RG;
			user_data->state = NONE_EFFECT;
			post_gpio_msg(user_data);
		};
		InnerTimer default_status(user_data->timeout, defaultFunc);
		break;
	}
}




