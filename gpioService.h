﻿#ifndef GPIO_SERVICE_H_
#define GPIO_SERVICE_H_

#include <map>
#include <wiringPi.h>

#include "WorkerThread.h"


struct GpioData;

class gpioService : public WorkerThread
{
	
public:
	int led_pin[3];
	int buzzer_pin;
	int buzzer_freq;
	
	enum Led_State
	{
		NONE_EFFECT,
		BLINKY,
		TOGGLE,
		TURN_OFF,
		TURN_ON,
	};
	
	enum GPIO
	{
		LED_RED,
		LED_GREEN,
		LED_BLUE,
		LED_RG,
		LED_RB,
		LED_GB,
		LED_RGB,
	};
	enum BuzzerState
	{
		BUZZER_ON = 0,
		BUZZER_OFF,
		
	};
	
	gpioService();
	gpioService(int red_pin, int green_pin, int blue_pin, int buzzer_pin, int buzzer_freq);

	//
	void gpio_control(GPIO led, Led_State state, unsigned int timeout);
	void gpio_control(GPIO led, Led_State state, unsigned int timeout, BuzzerState buzzer_state);
	void gpio_control_inner(GPIO led, Led_State state, unsigned timeout);
	//

	~gpioService() override;
	void ExitThread() override;
	void dispatch_process() override;
	void TimerThread() override;
	void post_gpio_timer_msg(std::shared_ptr<GpioData> msg);

private :
	void post_gpio_msg(std::shared_ptr<GpioData> msg);
	void post_gpio_inner_msg(std::shared_ptr<GpioData> msg, int timeout);
	void gpio_process_msg(std::shared_ptr<ThreadMsg> thread_msg);
	void gpio_led_blinky(GPIO led, Led_State state, int timeout);
	void gpio_turn_on(int pinMask[]);
	void gpio_turn_off(int pinMask[]);
	void gpio_toggle(int pinMask[]);
	int timeout_;
	int blinkyTimes;
	int buzzerTimer;
	int gpio_affected_[3];
 };

#endif
