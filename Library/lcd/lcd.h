#ifndef LCD_H__
#define LCD_H__

#include "wiringPiI2C.h"
#include "wiringPi.h"
#include <stdlib.h>
#include <stdio.h>
// Define some device parameters
#define I2C_ADDR   0x27 // I2C device address

	// Define some device constants
#define LCD_CHR  1 // Mode - Sending data
#define LCD_CMD  0 // Mode - Sending command

#define LINE1  0x80 // 1st line
#define LINE2  0xC0 // 2nd line

#define LCD_BACKLIGHT   0x08  // On
	// LCD_BACKLIGHT = 0x00  # Off
#define LCD_MAXLEN  16
#define ENABLE  0b00000100 // Enable bit
#ifdef __cplusplus
extern "C" {
#endif



	void lcd_init(void);
	void lcd_write_byte(int bits, int mode);
	void lcd_toggle_enable(int bits);

	// added by Lewis
	void lcd_type_int(int nData);
	void lcd_type_float(float nFloat);
	void lcd_set_location(int line);  //move cursor
	void lcd_clr_all(void);  // clr LCD return home
	void lcd_clr_line(int line);
	void lcd_write_line(const char *s);
	void lcd_writer_char(char val);
	void lcd_write_center(const char* s);

	
#ifdef __cplusplus
}
#endif
#endif /*LCD_H__*/