#include "lcdService.h"

#include <boost/asio/io_service.hpp>

#include "InnerTimer.h"
#include "Fault.h"
#include "wiringPiI2C.h"
#include <boost/thread/lock_types.hpp>
#include <boost/asio.hpp>
#include "lcd.h"
namespace asio = boost::asio;



using namespace std;

extern configMachine config;

extern int fd;

struct ThreadMsg
{
	ThreadMsg(int prior, int i, std::shared_ptr<void> m) { priority = prior; id = i; msg = std::move(m); }
	int priority;
	int id;
	std::shared_ptr<void> msg;
	bool operator<(const ThreadMsg&_threadMsg) const {
		return priority < _threadMsg.priority;  
	} 
};
struct LcdData
{
	lcdService::State state;
	std::string msg;
	lcdService::Effect effect;
	unsigned int timeout;
	uint8_t BlinkyTimes;
};
lcdService::lcdService()
{
	fd = wiringPiI2CSetupInterface("/dev/i2c-0", 0x27);
	lcd_init();


	this->lcd_mainScreen();
}


lcdService::lcdService(const char* threadName): WorkerThread(threadName)
{
	fd = wiringPiI2CSetupInterface("/dev/i2c-0", 0x27);
	lcd_init();
	this->timer_countdown = 10;
	this->timer_state_ = TIMER_SCREEN;
	std::shared_ptr<LcdData> lcdMsg(new LcdData());
	lcdMsg->state = TIME_SCREEN;
	lcdMsg->effect = NONE_EFFECT;
	lcdMsg->timeout = 100;
	this->CreateThread();
	this->PostLcdMsg(lcdMsg);
	//this->lcd_mainScreen();
}


lcdService::~lcdService()
{
	
}

void lcdService::lcd_display(std::string msg, State state, Effect effect, unsigned int timeout)
{
	std::shared_ptr<LcdData> lcdMsg(new LcdData());
	lcdMsg->state = state;
	lcdMsg->effect = effect;
	lcdMsg->timeout = timeout;
	lcdMsg->BlinkyTimes = 3;
	lcdMsg->msg = msg;

	
	this->PostLcdMsg(lcdMsg, 0);
}

void lcdService::lcd_mainScreen()
{
	char timeFormat[80];
	struct tm* tm;
	get_time(&tm);
	sprintf(timeFormat, "%02d:%02d:%02d", tm->tm_hour, tm->tm_min, tm->tm_sec);

	lcd_set_location(LINE1);
	lcd_write_line(config.device_msg_.msg_default.msg.c_str());
	lcd_set_location(LINE2);
	lcd_write_center(timeFormat);
}

void lcdService::lcd_welcomeScreen(std::string name_str)
{
	this->lcd_clear();
	lcd_set_location(LINE1);
	lcd_write_center(config.device_msg_.msg_tag_success.msg.c_str());
	lcd_set_location(LINE2);
	lcd_write_center(name_str.c_str());
}
void lcdService::lcd_RejectScreen()
{
	this->lcd_clear();
	lcd_set_location(LINE1);
	lcd_write_center(config.device_msg_.msg_tag_failed.msg.c_str());

}

void lcdService::lcd_semiboardingScreen(std::string name_str)
{
	this->lcd_clear();
	lcd_set_location(LINE1);
	lcd_write_line(config.device_msg_.msg_semi_success.msg.c_str());
	lcd_set_location(LINE2);
	lcd_write_center(name_str.c_str());
}

void lcdService::lcd_show_str(uint8_t line, std::string str)
{
	lcd_set_location(line);
	lcd_write_center(str.c_str());
}

void lcdService::lcd_k3_screen(k3_msg mode)
{
	this->lcd_clear();
	switch (mode)
	{
	case REMIND:

		lcd_set_location(LINE1);
		lcd_write_center(config.mK3_Config.k3_msg_remind.substr(0, 15).c_str());
		lcd_set_location(LINE2);
		lcd_write_center(config.mK3_Config.k3_msg_remind.substr(15, 12).c_str());
		break;
	case APPROVE:
		//Mode Normal
		//Da do nhiet do cho vao
		lcd_set_location(LINE1);
		lcd_write_center(config.mK3_Config.k3_msg_access.substr(0, 7).c_str());
		lcd_set_location(LINE2);
		lcd_write_center(config.mK3_Config.k3_msg_access.substr(7, 9).c_str());
		break;
	case REJECT:
		lcd_set_location(LINE1);
		lcd_write_center(config.mK3_Config.k3_msg_reject.substr(0, 13).c_str());
		lcd_set_location(LINE2);
		lcd_write_center(config.mK3_Config.k3_msg_reject.substr(13, 8).c_str());
		break;
	}
}


void lcdService::lcd_clear()
{
	lcd_clr_all();
}


void lcdService::lcd_tickTime()
{
	this->lcd_mainScreen();
}


void lcdService::lcd_process(std::shared_ptr<ThreadMsg> thread_msg)
{
	if (thread_msg == nullptr) return;

	const auto user_data = std::static_pointer_cast<LcdData>(thread_msg->msg);

	//std::cout << "Data process: " << user_data->state << "\r" << "Data msg: " << user_data->msg << std::endl;
	
	switch (user_data->state)
	{
		
	case TIME_SCREEN:
		this->lcd_mainScreen();
		break;
	case TAG_INVALID:
		this->lcd_clear();
		this->lcd_show_str(LINE1, "THE KHONG HOP LE");
		this->lcd_show_str(LINE2, "VUI LONG THU LAI");
		break;
	case TAG_WELCOME:
		this->lcd_welcomeScreen(user_data->msg);
		break;
	case TAG_REJECT:
		this->lcd_RejectScreen();
		break;
	case TAG_SEMIBOARDING:
		this->lcd_semiboardingScreen(user_data->msg);
		break;
	case K3_REMIND:
		this->lcd_k3_screen(REMIND);
		break;
	case K3_APPROVE:
		this->lcd_k3_screen(APPROVE);
		break;
	case K3_REJECT:
		this->lcd_k3_screen(REJECT);
		break;
	case CLEAR_SCR:
		this->lcd_clear();
		break;
	}
	
	switch (user_data->effect)
	{
	case NONE_EFFECT:
	{
		std::cout << "None effect" << std::endl;
		//Restart Timer
		std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(1, MSG_TIMER, 0));
		// Add timer msg to stack and notify worker thread
		std::unique_lock<std::mutex> lk(m_mutex);
		m_queue.push(threadMsg);
		m_cv.notify_one();
		break;
	}
	case BLINKY:
	{
		/*std::cout << "Blinky effect" << std::endl;
		auto callbackFunc = [&user_data, this]()
		{
			if (user_data->BlinkyTimes != 0) {
				if (user_data->BlinkyTimes % 2 == 0)
				{
					user_data->state = CLEAR_SCR;
				}
				else user_data->state = TAG_WELCOME;

				user_data->BlinkyTimes--;

				std::cout << "Post Data with: " << static_cast<unsigned int>(user_data->BlinkyTimes) << std::endl;
				this->PostLcdMsg(user_data);
			}
			else if (user_data->BlinkyTimes == 0)
			{
				std::cout << "Post No Blinky \r";
				user_data->state = TIME_SCREEN;
				user_data->effect = NONE_EFFECT;
				this->PostLcdMsg(user_data);
			}
		};
		InnerTimer blinkyTimer{ user_data->timeout, callbackFunc };*/
		break;
	}
	case STATIC_: 
		this->timer_countdown = user_data->timeout /200;
		std::cout << "Static character" << std::endl;
		std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(1, MSG_TIMER, 0));
		// Add timer msg to stack and notify worker thread
		std::unique_lock<std::mutex> lk(m_mutex);
		m_queue.push(threadMsg);
		m_cv.notify_one();
		
		break;
	}
}


void lcdService::dispatch_process()
{
	cout << "lcd thread start \r";
	this->m_timerExit = false;
	std::thread timerThread(&lcdService::TimerThread, this);

	while (true)
	{
		std::shared_ptr<ThreadMsg> thread_msg;
		{
			// Wait for a message to be added to the stack
			std::unique_lock<std::mutex> lk(m_mutex);
			while (m_queue.empty())
				m_cv.wait(lk);

			if (m_queue.empty())
				continue;

			thread_msg = std::move(m_queue.front());
			m_queue.pop();
		}
		switch (thread_msg->id)
		{
		case MSG_POST_USER_DATA:
		{
			this->m_timerExit = true;
			this->lcd_process(thread_msg);
			break;
		}
		case MSG_TIMER:
			//Tick every second -> lcd.main_screen
			//cout << "Timer expired on " << THREAD_NAME << endl;
			this->m_timerExit = false;
			break;
		case MSG_TIMER_TIMEOUT:
			this->lcd_mainScreen();
			break;
		case MSG_EXIT_THREAD:
			{
				this->m_timerExit = true;
				timerThread.join();
				return;
			}

		default:
			ASSERT();
		}
	}
}


void lcdService::PostLcdMsg(std::shared_ptr<LcdData> msg)
{
	ASSERT_TRUE(m_thread);

	// Create a new ThreadMsg
	std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(1,MSG_POST_USER_DATA, msg));

	// Add user data msg to queue and notify worker thread
	std::cout << "Post to stack \r";
	std::unique_lock<std::mutex> lk(m_mutex);
	m_queue.push(threadMsg);
	m_cv.notify_one();
}
void lcdService::PostLcdMsg(std::shared_ptr<LcdData> msg, int prior)
{
	ASSERT_TRUE(m_thread);
	//this->m_EffectExit = true;
	// Create a new ThreadMsg
	std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(prior,MSG_POST_USER_DATA, msg));
	
	// Add user data msg to queue and notify worker thread
	std::cout << "Post to stack \r";
	std::unique_lock<std::mutex> lk(m_mutex);
	
	m_queue.push(threadMsg);
	m_cv.notify_one();
}
void lcdService::TimerThread()
{
	while (true)
	{
		if(this->m_timerExit != false) continue;
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
		if (--this->timer_countdown != 0) continue;
		switch (this->timer_state_) {
		case TIMER_SCREEN:
		{
			this->timer_countdown = 5;
			std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(0, MSG_TIMER_TIMEOUT, 0));
			std::unique_lock<std::mutex> lk(m_mutex);
			m_queue.push(threadMsg);
			m_cv.notify_one();
			break;
		}
		case COUNTDOWN: 
			break;
		default: ;
		}
		
		
		// Sleep for 250mS then put a MSG_TIMER into the message queue
		//std::cout << "Timer Tick Tick \r";
		
	}
}

void lcdService::ExitThread()
{
	if (!m_thread)
		return;

	// Create a new ThreadMsg
	std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(0,MSG_EXIT_THREAD, 0));

	// Put exit thread message into the queue
	{
		lock_guard<mutex> lock(m_mutex);
		m_queue.push(threadMsg);
	
		m_cv.notify_one();
	}

	m_thread->join();
	m_thread = nullptr;
}
