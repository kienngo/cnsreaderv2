#ifndef LCDSERVICE_H__
#define LCDSERVICE_H__


#include <atomic>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>

#include <queue>
#include <thread>
#include "configMachine.hpp"
#include "ultils.h"
#include "WorkerThread.h"
#include "Library/lcd/lcd.h"
#include <boost/signals2.hpp>

struct LcdData;


class lcdService : public WorkerThread
{
public:
	enum State
	{
		TIME_SCREEN = 0,
		TAG_INVALID,
		TAG_WELCOME,
		TAG_REJECT,
		TAG_SEMIBOARDING,
		K3_REMIND,
		K3_APPROVE,
		K3_REJECT,
		CLEAR_SCR
	};

	enum Effect
	{
		NONE_EFFECT,
		BLINKY,
		STATIC_
	};

	enum TimerState
	{
		TIMER_SCREEN,
		COUNTDOWN
	};
	enum k3_msg
	{
		REMIND,
		APPROVE,
		REJECT
	};

	
	
	lcdService();
	lcdService(const char* threadName);


	~lcdService();
	
	
	void lcd_display(std::string msg = std::string(), State state = TAG_WELCOME, Effect effect = NONE_EFFECT, unsigned int timeout = 200);

	//Inheritance
	void dispatch_process() override;
	void TimerThread() override;
	void ExitThread() override;
	std::atomic<bool> m_EffectExit;
private:
	void lcd_mainScreen();
	void lcd_welcomeScreen(std::string name_str);
	void lcd_RejectScreen();
	void lcd_semiboardingScreen(std::string name_str);
	void lcd_show_str(uint8_t line, std::string str);
	void lcd_k3_screen(k3_msg mode);
	void lcd_clear();
	void lcd_tickTime();
	void lcd_process(std::shared_ptr<ThreadMsg> thread_msg);
	void PostLcdMsg(std::shared_ptr<LcdData> msg);
	void PostLcdMsg(std::shared_ptr<LcdData> msg, int prior);
	
	lcdService(const lcdService&) = delete;
	lcdService& operator=(const lcdService&) = delete;
	TimerState timer_state_;
	unsigned int timer_countdown;
	//boost::signals2::signal<void> _signal;
	//std::stack<std::shared_ptr<ThreadMsg>> m_stack;
};

#endif
