// float to string
#include "lcd.h"
#include <string.h>
//#include <cstdint>

int fd;   // seen by all subroutines

void lcd_type_float(float nFloat) {
	char buffer[20];
	sprintf(buffer, "%4.2f", nFloat);
	lcd_write_line(buffer);
}

// int to string
void lcd_type_int(int nData) {
	char array1[20];
	sprintf(array1, "%d", nData);
	lcd_write_line(array1);
}

// clr lcd go home loc 0x80
void lcd_clr_all(void) {
	lcd_write_byte(0x01, LCD_CMD);
	lcd_write_byte(0x02, LCD_CMD);
}
void lcd_clr_line(int line)
{

}
// go to location on LCD
void lcd_set_location(int line) {
	lcd_write_byte(line, LCD_CMD);
}

// out char to LCD at current position
void lcd_writer_char(char val) {

	lcd_write_byte(val, LCD_CHR);
}


void lcd_write_center(const char* s)
{
	int len = strlen(s);
	char temp[16] = {};
	if (len > LCD_MAXLEN) {
		len = LCD_MAXLEN;
		//strncpy(temp, s, len);
	}
	else if(len == LCD_MAXLEN) {
		lcd_write_line(s);
		return;
	}
	int begin_cursor = LCD_MAXLEN/2 - len / 2;
	//printf("begin_cursor %d len %d", begin_cursor, len);
	for (int Index = 0; Index < begin_cursor; Index++)
	{
		lcd_write_line(" ");
	}
	lcd_write_line(s);
	for (int Index = begin_cursor + len; Index < LCD_MAXLEN; Index++)
	{
		lcd_write_line(" ");
	}
}

// this allows use of any size string
void lcd_write_line(const char *s) {

	while (*s) lcd_write_byte(*(s++), LCD_CHR);

}

void lcd_write_byte(int bits, int mode) {

	//Send byte to data pins
	// bits = the data
	// mode = 1 for data, 0 for command
	int bits_high;
	int bits_low;
	// uses the two half byte writes to LCD
	bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT;
	bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT;

	// High bits
	wiringPiI2CReadReg8(fd, bits_high);
	lcd_toggle_enable(bits_high);

	// Low bits
	wiringPiI2CReadReg8(fd, bits_low);
	lcd_toggle_enable(bits_low);
}

void lcd_toggle_enable(int bits) {
	// Toggle enable pin on LCD display
	delayMicroseconds(500);
	wiringPiI2CReadReg8(fd, (bits | ENABLE));
	delayMicroseconds(500);
	wiringPiI2CReadReg8(fd, (bits & ~ENABLE));
	delayMicroseconds(500);
}


void lcd_init() {
	// Initialise display
	lcd_write_byte(0x33, LCD_CMD);  // Initialise
	lcd_write_byte(0x32, LCD_CMD);  // Initialise
	lcd_write_byte(0x06, LCD_CMD);  // Cursor move direction
	lcd_write_byte(0x0C, LCD_CMD);  // 0x0F On, Blink Off
	lcd_write_byte(0x28, LCD_CMD);  // Data length, number of lines, font size
	lcd_write_byte(0x01, LCD_CMD);  // Clear display
	delayMicroseconds(500);
}