#include "configMachine.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
using namespace Json;
using namespace std;
/**********************************************************************************************//**
 * \fn	void configMachine::setDeviceSite(std::string sMachineSite)
 *
 * \brief	Sets device site
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	sMachineSite	The machine site.
 **************************************************************************************************/

void configMachine::setDeviceSite(std::string sMachineSite)
{
	cout << "device site: " << sMachineSite << '\n'; 	
	this->sMachineSite = sMachineSite;
}
void configMachine::setSetTempSafetyLevel(uint16_t nTempSafetyLevel)
{
	this->nTempSafetyLevel = nTempSafetyLevel;
}
void configMachine:: setEnaTempSafety(bool bEnaTempSafety)
{
	this->bEnaTempSafety = bEnaTempSafety;
}
void configMachine::setEnaAccessControl(bool bEnaAccessControl)
{
	if (bEnaAccessControl) cout << "enable access control \n";
	this->bEnaAccessControl = bEnaAccessControl;
}

void configMachine::setEnaSemiBoarding(bool bEnaSemiBoarding)
	
{
	if (bEnaAccessControl) cout << "enable semi boarding \n";
	this->bEnaSemiBoarding = bEnaSemiBoarding;
}

void configMachine::setDirAttendance(std::string sDirAttendance)
{
	this->sDirAttendance = sDirAttendance;
}

void configMachine::setDirAttendanceTemp(std::string sDirAttendanceTemp)
{
	this->sDirAttendanceTemp = sDirAttendanceTemp;
}

void configMachine::setDirEvent(std::string sDirEvent)
{
	this->sDirEvent = sDirEvent;
}

void configMachine::setAccessCmdTimeout(uint32_t nAccessCmdTimeout)
{
	this->nAccessCmdTimeout = nAccessCmdTimeout;
}

void configMachine::setAccessDefaultMsg(std::string sAccessDefaultMsg)
{
	this->sAccessDefaultMsg = sAccessDefaultMsg;
}

void configMachine::setAccessSuccessMsg(std::string sAccessSuccessMsg)
{
	this->sAccessSuccessMsg = sAccessSuccessMsg;
}

void configMachine::setAccessFailedMsg(std::string sAccessFailedMsg)
{
	this->sAccessFailedMsg = sAccessFailedMsg;
}


void configMachine::setSemiBoardingMsg(std::string sSemiBoardingMsg)
{
	this->sSemiBoardingMsg = sSemiBoardingMsg;
}

void configMachine::setBlinkyLedTimeout(uint32_t nBlinkyLedTimeout)
{
	this->nBlinkyLedTimeout = nBlinkyLedTimeout;
}


void configMachine::setBuzzerTimeout(uint32_t nBuzzerTimeout)
{
	this->nBuzzerTimeout = nBuzzerTimeout;
}

void configMachine::setAttendanceDuty(uint32_t nAttendanceDuty)
{
	this->nAttendanceDuty = nAttendanceDuty;
}

void configMachine::setKeepaliveDuty(uint32_t nKeepaliveDuty)
{
	this->nKeepaliveDuty = nKeepaliveDuty;
}

void configMachine::setWhitelistDuty(uint32_t nWhitelistDuty)
{
	//std::cout << nWhitelistDuty << "good job" << '\n';
	this->nWhitelistDuty = nWhitelistDuty;
}

void configMachine::setDeviceID(std::string sDeviceID)
{
	this->sDeviceID = sDeviceID;
}
std::string configMachine::getMachineSite(void)
{
	return this->sMachineSite;
}

/**********************************************************************************************//**
 * \fn	void configMachine::setTokenKeepAlive(std::string sTokenKeepAlive)
 *
 * \brief	Sets token keep alive
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	sTokenKeepAlive	The token keep alive.
 **************************************************************************************************/

void configMachine::setTokenKeepAlive(std::string sTokenKeepAlive)
{
	configMachine::sTokenKeepAlive = sTokenKeepAlive;
}

/**********************************************************************************************//**
 * \fn	void configMachine::setTokenWhiteList(std::string sTokenWhiteList)
 *
 * \brief	Sets token white list
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	sTokenWhiteList	List of token whites.
 **************************************************************************************************/

void configMachine::setTokenWhiteList(std::string sTokenWhiteList)
{
	configMachine::sTokenWhiteList = sTokenWhiteList;
}

/**********************************************************************************************//**
 * \fn	void configMachine::setWhiteListURL(std::string sWhiteListURL)
 *
 * \brief	Sets white list URL
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	sWhiteListURL	URL of the white list.
 **************************************************************************************************/

void configMachine::setWhiteListURL(std::string sWhiteListURL)
{
	configMachine::sWhiteListURL = sWhiteListURL;
}

/**********************************************************************************************//**
 * \fn	void configMachine::setKeepAliveURL(std::string sKeepAliveURL)
 *
 * \brief	Sets keep alive URL
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	sKeepAliveURL	URL of the keep alive.
 **************************************************************************************************/

void configMachine::setKeepAliveURL(std::string sKeepAliveURL)
{
	configMachine::sKeepAliveURL = sKeepAliveURL;
}

/**********************************************************************************************//**
 * \fn	void configMachine::setPostAttendanceURL(std::string sPostAttendanceURL)
 *
 * \brief	Sets post attendance URL
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	sPostAttendanceURL	URL of the post attendance.
 **************************************************************************************************/

void configMachine::setPostAttendanceURL(std::string sPostAttendanceURL)
{
	configMachine::sPostAttendanceURL = sPostAttendanceURL;
}



void configMachine::setPostSemiBoardingURL(std::string sPostSemiBoardingURL)
{
	this->sPostSemiBoardingURL = sPostSemiBoardingURL;
}

/**********************************************************************************************//**
 * \fn	void configMachine::setUpConfig(const char *site)
 *
 * \brief	Sets up the configuration
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	site	The site.
 **************************************************************************************************/

void configMachine::setUpConfig(const char* site)
{

	//stringvec listDir;
	//list_directory(".", listDir); //Get list directory at file executed


	//std::fstream jsConfigHandle;
	std::ifstream jsConfigHandle("/home/CnsReader/config/config.json");
	//jsConfigHandle.open("/home/CnsReader/config/config.json", std::fstream::in);

	if (!jsConfigHandle.is_open())
	{
		return;
	}
	Value js_config_content;
	Reader reader;
	reader.parse(jsConfigHandle, js_config_content);
	
	//jsConfigHandle >> js_config_content;

	jsConfigHandle.close();

	std::cout << js_config_content << '\n';


	setDeviceSite(site);
	setDeviceID(js_config_content[site]["DEVICE_ID"].asString());
	setEnaAccessControl(js_config_content[site]["ENABLE_ACCESSCONTROL"].asBool());
	setEnaTempSafety(js_config_content[site]["ENABLE_TEMPSAFETY"].asBool());

	setDirAttendance(js_config_content[site]["DIR_ATTENDANCE"].asString());
	setDirAttendanceTemp(js_config_content[site]["DIR_ATTENDANCE_TEMP"].asString());
	setDirEvent(js_config_content[site]["DIR_EVENT"].asString());
	setAccessCmdTimeout(js_config_content[site]["ACCESS_MSG_TIMEOUT"].asUInt());
	setAccessDefaultMsg(js_config_content[site]["ACCESS_MSG_DEFAULT"].asString());
	setAccessSuccessMsg(js_config_content[site]["ACCESS_SUCESS_MSG"].asString());
	setAccessFailedMsg(js_config_content[site]["ACCESS_FAIL_MSG"].asString());
	setSemiBoardingMsg(js_config_content[site]["SEMIBOARDING_MSG"].asString());
	setBlinkyLedTimeout(js_config_content[site]["BLINK_LED_TIMEOUT"].asUInt());
	setBuzzerTimeout(js_config_content[site]["BUZZER_TIMEOUT"].asUInt());
	setAttendanceDuty(js_config_content[site]["POSTATTENDANCE_TIME_DURATION"].asUInt());
	setKeepaliveDuty(js_config_content[site]["KEEPALIVE_TIME_DURATION"].asUInt());
	setWhitelistDuty(js_config_content[site]["WHITELIST_TIME_DURATION"].asUInt());
	setTokenWhiteList(js_config_content[site]["TOKEN_WHITELIST"].asString());
	setTokenKeepAlive(js_config_content[site]["TOKEN_KEEPALIVE"].asString());
	setWhiteListURL(js_config_content[site]["URL_WHITELIST"].asString());
	setKeepAliveURL(js_config_content[site]["URL_KEEP_ALIVE"].asString());
	setPostAttendanceURL(js_config_content[site]["URL_POST_ATTENDANCE"].asString());
	setPostSemiBoardingURL(js_config_content[site]["URL_POST_SEMIBOARDING"].asString());

	//Temp
	setSetTempSafetyLevel(js_config_content[site]["SET_TEMPSAFETYLEVEL"].asUInt());
	setEnaSemiBoarding(js_config_content[site]["ENABLE_SEMIBOARDING_ACCESS"].asBool());
	mK3_Config.AlarmUpperLimit = js_config_content[site]["external_device_msg"]["K3_Pro"]["ALARM_UPPER_LIMIT"].asFloat();
	mK3_Config.output = js_config_content[site]["external_device_msg"]["K3_Pro"]["OUTPUT_SIGNAL"].asUInt();
	mK3_Config.mode = js_config_content[site]["external_device_msg"]["K3_Pro"]["MODE"].asUInt();
	mK3_Config.TempFormat = js_config_content[site]["external_device_msg"]["K3_Pro"]["TEMPFORMAT"].asUInt();
	mK3_Config.k3_msg_access = js_config_content[site]["external_device_msg"]["K3_Pro"]["K3_MSG_ACCESS"].asString();
	mK3_Config.k3_msg_remind = js_config_content[site]["external_device_msg"]["K3_Pro"]["K3_MSG_REMIND"].asString();
	mK3_Config.k3_msg_reject = js_config_content[site]["external_device_msg"]["K3_Pro"]["K3_MSG_REJECT"].asString();
	mK3_Config.k3_measure_status = TEMP_NOT_MEASURED;
}

void configMachine::set_up_config(const char* site)
{
	std::ifstream jsConfigHandle("/home/CnsReader/config/config1.json");

	if(!jsConfigHandle.is_open())
	{
		return;
	}
	Value js_config_content;
	Reader reader;
	reader.parse(jsConfigHandle, js_config_content);

	jsConfigHandle.close();

	std::cout << js_config_content << '\n';

	//Config
	config_.samId_ = js_config_content[site]["config"]["samId"].asString();
	config_.mcpConfig_.terminalId = js_config_content[site]["config"]["VinaIDConfig"]["terminalId"].asString();
	config_.mcpConfig_.transactionToken = js_config_content[site]["config"]["VinaIDConfig"]["transactionToken"].asString();
	config_.mcpConfig_.attendanceToken = js_config_content[site]["config"]["VinaIDConfig"]["attendanceToken"].asString();
	//file dir
    file_sys_dir_.dir_whitelist = js_config_content[site]["file_sys_dir"]["dir_whitelist"].asString();
	file_sys_dir_.dir_attendance = js_config_content[site]["file_sys_dir"]["dir_attendance"].asString();
	file_sys_dir_.dir_attendance_temp = js_config_content[site]["file_sys_dir"]["dir_attendance_temp"].asString();
	file_sys_dir_.dir_semiboard = js_config_content[site]["file_sys_dir"]["dir_semiboarding"].asString();
	file_sys_dir_.dir_semiboard_temp = js_config_content[site]["file_sys_dir"]["dir_semiboarding_temp"].asString();
	file_sys_dir_.dir_event = js_config_content[site]["file_sys_dir"]["dir_event"].asString();
	//url
	url_.url_whitelist = js_config_content[site]["url"]["url_whitelist"].asString();
	url_.url_keep_alive = js_config_content[site]["url"]["url_keep_alive"].asString();
	url_.url_attendance = js_config_content[site]["url"]["url_post_attendance"].asString();
	url_.url_semi_boarding = js_config_content[site]["url"]["url_post_semiboarding"].asString();
	url_.url_delete_notification = js_config_content[site]["url"]["url_delete_notification"].asString();
	//device
	device.bEnaAccessControl = js_config_content[site]["device"]["enable_accesscontrol"].asBool();
	device.bEnaSemiBoarding = js_config_content[site]["device"]["enable_semiboarding_access"].asBool();
	device.bEnaTempSafety = js_config_content[site]["device"]["enable_tempsafety"].asBool();
	if (device.bEnaTempSafety)
	{
		device.nTempSafetyLevel = static_cast<uint8_t>(js_config_content[site]["device"]["set_tempsafetylevel"].asUInt());
	}
	device.nAttendanceDuty = js_config_content[site]["device"]["postattendance_time"].asUInt();
	device.nKeepaliveDuty = js_config_content[site]["device"]["keep_alive_time"].asUInt();
	device.nWhitelistDuty = js_config_content[site]["device"]["whitelist_time"].asUInt();
	
	//device msg
    device_msg_.msg_default.msg = js_config_content[site]["message"]["default_msg"]["msg"].asString();
	device_msg_.msg_default.led = js_config_content[site]["message"]["default_msg"]["led"].asString();
	device_msg_.msg_default.timeout = js_config_content[site]["message"]["default_msg"]["timeout"].asUInt();

	device_msg_.msg_device_failed.msg = js_config_content[site]["message"]["device_failed"]["msg"].asString();
	device_msg_.msg_device_failed.led = js_config_content[site]["message"]["device_failed"]["led"].asString();
	device_msg_.msg_device_failed.timeout = js_config_content[site]["message"]["device_failed"]["timeout"].asUInt();

	device_msg_.msg_tag_success.msg = js_config_content[site]["message"]["tag_success"]["msg"].asString();
	device_msg_.msg_tag_success.led = js_config_content[site]["message"]["tag_success"]["led"].asString();
	device_msg_.msg_tag_success.timeout = js_config_content[site]["message"]["tag_success"]["timeout"].asUInt();

	device_msg_.msg_semi_success.msg = js_config_content[site]["message"]["tag_semi_success"]["msg"].asString();
	device_msg_.msg_semi_success.led = js_config_content[site]["message"]["tag_semi_success"]["led"].asString();
	device_msg_.msg_semi_success.timeout = js_config_content[site]["message"]["tag_semi_success"]["timeout"].asUInt();

	device_msg_.msg_tag_failed.msg = js_config_content[site]["message"]["tag_failed"]["msg"].asString();
	device_msg_.msg_tag_failed.led = js_config_content[site]["message"]["tag_failed"]["led"].asString();
	device_msg_.msg_tag_failed.timeout = js_config_content[site]["message"]["tag_failed"]["timeout"].asUInt();


	mK3_Config.AlarmUpperLimit = js_config_content[site]["external_device"]["temperature"]["alarm_upper_limit"].asFloat();
	mK3_Config.output = js_config_content[site]["external_device"]["temperature"]["output_signal"].asUInt();
	mK3_Config.mode = js_config_content[site]["external_device"]["temperature"]["mode"].asUInt();
	mK3_Config.TempFormat = js_config_content[site]["external_device"]["temperature"]["tempformat"].asUInt();
	mK3_Config.k3_msg_access = js_config_content[site]["external_device"]["temperature"]["msg_access"].asString();
	mK3_Config.k3_msg_remind = js_config_content[site]["external_device"]["temperature"]["msg_remind"].asString();
	mK3_Config.k3_msg_reject = js_config_content[site]["external_device"]["temperature"]["msg_reject"].asString();
	mK3_Config.default_timeout = js_config_content[site]["external_device"]["temperature"]["timeout"].asUInt();
	mK3_Config.remind_timeout = js_config_content[site]["external_device"]["temperature"]["timeout_reject"].asUInt();
	mK3_Config.reject_timeout = js_config_content[site]["external_device"]["temperature"]["timeout_reject"].asUInt();
	mK3_Config.k3_measure_status = TEMP_NOT_MEASURED;
	//external
	
}

/**********************************************************************************************//**
 * \fn	configMachine::configMachine()
 *
 * \brief	Default constructor
 *
 * \author	Zeder
 * \date	6/25/2020
 **************************************************************************************************/

configMachine::configMachine()
{
}

/**********************************************************************************************//**
 * \fn	configMachine::configMachine(const char *site)
 *
 * \brief	Constructor
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	site	The site.
 **************************************************************************************************/

configMachine::configMachine(const char* site)
{
	//setUpConfig(site);
	set_up_config(site);
}
