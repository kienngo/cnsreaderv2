#ifndef __CONFIG_MACHINE_H
#define __CONFIG_MACHINE_H
#include <cstdint>
#include <getopt.h>
#include <unistd.h>
#include <bits/stdc++.h> 
#include <sys/ioctl.h>
#include "K3Machine.h"


typedef struct McpConfig
{
	std::string terminalId;
	std::string transactionToken;
	std::string attendanceToken;
};
typedef struct Config_t {
	std::string samId_;
	McpConfig mcpConfig_;
};
typedef struct File_sys_dir
{
	std::string dir_whitelist;
	std::string dir_attendance;
	std::string dir_attendance_temp;
	std::string dir_semiboard;
	std::string dir_semiboard_temp;
	std::string dir_event;
};

typedef struct Url_
{
	std::string url_whitelist;
	std::string url_keep_alive;
	std::string url_attendance;
	std::string url_semi_boarding;
	std::string url_delete_notification;
};

typedef struct Device_
{
	bool bEnaAccessControl;
	bool bEnaSemiBoarding;
	bool bEnaTempSafety;
	uint8_t nTempSafetyLevel;
	std::string externalUsb;
	std::string externalUsbCmd;
	
	uint32_t nAttendanceDuty;
	uint32_t nKeepaliveDuty;
	uint32_t nWhitelistDuty;
	
};

typedef struct Message
{
	std::string msg;
	std::string led;
	unsigned int timeout;
};

typedef struct DeviceMsg
{
	Message msg_default;
	Message msg_device_failed;
	Message msg_tag_success;
	Message msg_semi_success;
	Message msg_tag_failed;
};

typedef struct external_device
{
	K3Proconfig mK3_Config;
};


class configMachine
{
	

public:
	
	//New Config
	Config_t config_;
	File_sys_dir file_sys_dir_;
	Url_ url_;
	Device_ device;
	DeviceMsg device_msg_;
	//
	K3Proconfig mK3_Config;
	std::string sMachineSite;
	std::string sDeviceID;
	bool bEnaAccessControl;
	bool bEnaTempSafety;
	uint16_t nTempSafetyLevel;
	bool bEnaSemiBoarding;
	std::string sDirAttendance;
	std::string sDirAttendanceTemp;
	std::string sDirEvent;
	uint32_t nAccessCmdTimeout;
	std::string sAccessDefaultMsg;
	std::string sAccessSuccessMsg;
	std::string sAccessFailedMsg;
	std::string sSemiBoardingMsg;
	uint32_t nBlinkyLedTimeout;
	uint32_t nBuzzerTimeout;
	uint32_t nAttendanceDuty;
	uint32_t nKeepaliveDuty;
	uint32_t nWhitelistDuty;
	std::string sTokenKeepAlive;
	std::string sTokenWhiteList;
	std::string sWhiteListURL;
	std::string sKeepAliveURL;
	std::string sPostAttendanceURL;
	std::string sPostSemiBoardingURL;
	std::string sDeleteNotificationURL;
	void setDeviceSite(std::string machineSite);
	void setEnaTempSafety(bool bEnaTempSafe);
	void setSetTempSafetyLevel(uint16_t nTempSafetyLevel);
	void setEnaAccessControl(bool bEnaAccessControl);
	void setEnaSemiBoarding(bool bEnaSemiBoarding);
	void setDirAttendance(std::string sDirAttendance);
	void setDirAttendanceTemp(std::string sDirAttendanceTemp);
	void setDirEvent(std::string sDirEvent);
	void setAccessCmdTimeout(uint32_t nAccessCmdTimeout);
	void setAccessDefaultMsg(std::string sAccessDefaultMsg);
	void setAccessSuccessMsg(std::string sAccessSuccessMsg);
	void setAccessFailedMsg(std::string sAccessFailedMsg);
	void setSemiBoardingMsg(std::string sSemiBoardingMsg);
	void setBlinkyLedTimeout(uint32_t nBlinkyLedTimeout);
	void setBuzzerTimeout(uint32_t nBuzzerTimeout);
	void setAttendanceDuty(uint32_t nAttendanceDuty);
	void setKeepaliveDuty(uint32_t nKeepaliveDuty);
	void setWhitelistDuty(uint32_t nWhitelistDuty);
	void setDeviceID(std::string deviceID);
	void setTokenKeepAlive(std::string sTokenKeepAlive);
	void setTokenWhiteList(std::string sTokenWhiteList);
	void setWhiteListURL(std::string sWhiteListURL);
	void setKeepAliveURL(std::string sKeepAliveURL);
	void setPostAttendanceURL(std::string sPostAttendanceURL);
	void setPostSemiBoardingURL(std::string sPostSemiBoardingURL);
	std::string getMachineSite(void);


	void setUpConfig(const char* site);
	void set_up_config(const char* site);
	configMachine();
	configMachine(const char* site);
private:


};
#endif
//void setUpConfig(const char *jsConfig);