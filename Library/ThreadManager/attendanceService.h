#ifndef _ATTENDANCESERVICE_H
#define _ATTENDANCESERVICE_H
#include <configMachine.hpp>
#include <httpService.h>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <ultils.h>
#include <boost/chrono.hpp>
#include <boost/thread.hpp>

#include "wiringPi.h"
void post_attendace();
void post_semiboarding();
void* postattendanceService(void* arg);
#endif