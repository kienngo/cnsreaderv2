#include "attendanceService.h"

#include <string>

#include "configMachine.hpp"

extern configMachine config;
extern std::mutex attendance_mutex;
extern std::mutex semi_mutex;
/**********************************************************************************************//**
 * \fn	void* post_attendace_function(void* arg)
 *
 * \brief	Posts an attendance function
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **************************************************************************************************/


uint8_t log_attendance_bs = 0;
void post_attendace()
{
	printf("post normal attendance thread \n");
//	uint8_t result;

//	char* log_file_buffer;
	// long numbytes;
	// long num_card;
//	uint32_t time_interval = 0;
	unsigned int now;
	char log_[100];
	time_t time_ = time(nullptr);
	
	const char* log_atten_path = config.file_sys_dir_.dir_attendance.c_str();
	const char* log_atten_temp = config.file_sys_dir_.dir_attendance_temp.c_str();
	for (;;)
	{
		std::cout << "post normal attendance check" << '\n'; 
		msleep(config.device.nAttendanceDuty);

	    copy_contents_file(log_atten_path, log_atten_temp);

	    remove(log_atten_path);
		
		
		std::fstream log_temp(log_atten_temp);
		if (!log_temp.is_open())
		{
			std::cout << "File could not be opened " << '\n';
			continue;
		}
		
		log_temp.seekg(0, std::ios_base::end);		
		auto length = log_temp.tellg();
		log_temp.seekg(0, std::ios_base::beg);
		
		if (0 == length)
		{
			continue;
		}
		
		
		length = length / 35;
//		std::cout << "length  " << length << '\n';
		
		auto max_for = length / 20;
//		std::cout << "max for " << max_for << '\n';
		
		auto line = std::string {};
		auto body = std::string {};
		auto line_for_save = std::string { };
		
		
		if (1 == length)
		{
			std::cout << "only 1 card for att\n";
			std::getline(log_temp, line);
			body.append(R"({"deviceId":")");
			body.append(config.config_.mcpConfig_.terminalId);
			body.append(R"(","cardNumber":")");
			body.append(line.substr(0, 19));
			body.append(R"(","dateTime":")");
			body.append(line.substr(20, 14));
			body.append(R"(","cusTemperature":")");
			body.append(line.substr(35, line.length() - 35));
			body.append("\"}");
			
			auto rescode = post_attendance((char*)body.c_str());
			body.clear();
			log_temp.close();
			
			if (200 == rescode)
			{
				std::cout << "POST NORMAL OK ! " << '\n';
				
				remove(log_atten_temp);
			}
			else {
				printf("post normal failed\n");
				std::lock_guard<std::mutex> guard(attendance_mutex);
				FILE* ptr = fopen(log_atten_path, "a");
				if (nullptr == ptr)
				{
					FILE* ptr = fopen(log_atten_path, "a+");
				}
				fprintf(ptr, "%s\n", line.c_str());
				fclose(ptr);
				//			
				char log_[100];
				time_t time_ = time(nullptr);
				struct tm* tm = localtime(&time_);
				sprintf(log_, "post attendance failed with rescode %d at %s", rescode, asctime(tm));
				log_reader("post attendance", log_);
			}
			continue;
		}
		
		
		uint8_t line_remove = 0;
		for (auto j  = 0; j < length; j++)
		{				
			std::getline(log_temp, line);
			if (!line.empty())
			{
				line_remove++;
				line_for_save.append(line);
				line_for_save.append("\n");
			
				auto tmp_card = line.substr(0, 19);
				auto time =  line.substr(20, 14);
				auto temp = line.substr(35, line.length() - 35);
				body.append(R"({"deviceId":")");
				body.append(config.config_.mcpConfig_.terminalId);
				body.append(R"(","cardNumber":")");
				body.append(tmp_card);
				body.append(R"(","dateTime":")");
				body.append(time);
				body.append(R"(","cusTemperature":")");
				body.append(temp);
				
				if (((line_remove == 10) || (j == ((int)length - 1))) && (j != 0))
				{
					body.append("\"}");
					std::cout << body << '\n';
					auto rescode = post_attendance((char*)body.c_str());
					body.clear();
					//rescode = 500;
					if (200 == rescode)
					{
						std::cout << "POST NORMAL OK ! " << '\n';
						log_temp.close();
						printf("line remove: %d \n", line_remove);
						delete_line(log_atten_temp, line_remove);
						line_remove = 0;
						log_temp.open(log_atten_temp);
						
						
					}
					else {
						printf("post normal failed\n");
				
						std::lock_guard<std::mutex> guard(attendance_mutex);
			
						FILE* ptr = fopen(log_atten_path, "a");
						if (nullptr == ptr)
						{
							FILE* ptr = fopen(log_atten_path, "a+");
							//							continue;
						}
						std::cout << line_for_save.size() << '\n';
						std::cout << "line for save: " << '\n' << line_for_save;
						fprintf(ptr, "%s", line_for_save.c_str());
						fclose(ptr);

						//			
						
						struct tm* tm = localtime(&time_);
						sprintf(log_, "post attendance failed with rescode %d at %s", rescode, asctime(tm));
						log_reader("post attendance", log_);
						
						log_temp.close();
						printf("line remove: %d \n", line_remove);
						delete_line(log_atten_temp, line_remove);
						line_remove = 0;
						log_temp.open(log_atten_temp);
					}	
					line_for_save.clear();
				}
				else {
					body.append("\"},");
				}
			}
		}
		remove(log_atten_temp);
		

	}
}


void post_semiboarding() {
	printf("post semi attendance \n");
	uint8_t result;

	char* log_file_buffer;
	long numbytes;
	long num_card;
	uint32_t time_interval = 0;
	unsigned int now;
	char log_[100];
	time_t time_ = time(nullptr);

	
	const char* log_atten_path = config.file_sys_dir_.dir_semiboard.c_str();
	const char* log_atten_temp = config.file_sys_dir_.dir_semiboard_temp.c_str();
	for (;;)
	{
		std::cout << "post semiboarding attendance check----------------" << '\n';
		msleep(config.device.nAttendanceDuty);
		copy_contents_file(log_atten_path, log_atten_temp);
	
		remove(log_atten_path);


		std::fstream log_temp(log_atten_temp);
		if (!log_temp.is_open())
		{
			std::cout << "File could not be opened " << '\n';
			continue;
		}

		log_temp.seekg(0, std::ios_base::end);
		auto length = log_temp.tellg();
		log_temp.seekg(0, std::ios_base::beg);

		if (0 == length)
		{
			continue;
		}


		length = length / 35;
//		std::cout << "length  " << length << '\n';

		auto max_for = length / 20;
//		std::cout << "max for " << max_for << '\n';

		auto line = std::string {};
		auto body = std::string {};
		auto line_for_save = std::string { };


		if (1 == length)
		{
			std::cout << "only 1 card for semiboarding\n";
			std::getline(log_temp, line);
			body.append(R"({"deviceId":")");
			body.append(config.config_.mcpConfig_.terminalId);
			body.append(R"(","cardNumber":")");
			body.append(line.substr(0, 19));
			body.append(R"(","dateTime":")");
			body.append(line.substr(20, 14));
			body.append(R"(","message":"Nhan suat an"})");
//			body.append("}");

			auto rescode = post_attendance_semiboarding((char*)body.c_str());
			body.clear();
			log_temp.close();

			if (200 == rescode)
			{
				std::cout << "POST SEMIBOARDING ATTENDANCE OK ! " << '\n';

				remove(log_atten_temp);
			}
			else {
				printf("post SEMIBOARDING failed\n");
				
				std::lock_guard<std::mutex> guard(semi_mutex);
				
				FILE* ptr = fopen(log_atten_path, "a");
				if (nullptr == ptr)
				{
					FILE* ptr = fopen(log_atten_path, "a+");
				}
				fprintf(ptr, "%s\n", line.c_str());
				fclose(ptr);
				//			
				char log_[100];
				time_t time_ = time(nullptr);
				struct tm* tm = localtime(&time_);
				sprintf(log_, "post attendance failed with rescode %d at %s", rescode, asctime(tm));
				log_reader("post attendance", log_);
			}
			continue;
		}


		uint8_t line_remove = 0;
		for (auto j = 0; j < length; j++)
		{
			std::getline(log_temp, line);
			if (!line.empty())
			{
				line_remove++;
				line_for_save.append(line);
				line_for_save.append("\n");
				auto tmp_card = line.substr(0, 19);
				auto time = line.substr(20, 14);
				body.append(R"({"deviceId":")");
				body.append(config.config_.mcpConfig_.terminalId);
				body.append(R"(","cardNumber":")");
				body.append(tmp_card);
				body.append(R"(","dateTime":")");
				body.append(time);
				body.append(R"(","message":"Nhan suat an)");
				
				if (((line_remove == 10) || (j == ((int)length - 1))) && (j != 0))
				{
					body.append("\"}");
					std::cout << body << '\n';
					auto rescode = post_attendance_semiboarding((char*)body.c_str());
					body.clear();
					if (200 == rescode)
					{
						std::cout << "POST SEMI OK ! " << '\n';
						log_temp.close();
						printf("line remove: %d \n", line_remove);
						delete_line(log_atten_temp, line_remove);
						line_remove = 0;
						log_temp.open(log_atten_temp);


					}
					else {
						printf("post semi failed\n");

						std::lock_guard<std::mutex> guard(semi_mutex);

						FILE* ptr = fopen(log_atten_path, "a");
						if (nullptr == ptr)
						{
							FILE* ptr = fopen(log_atten_path, "a+");
							//							continue;
						}
						std::cout << line_for_save.size() << '\n';
						std::cout << "line semi for save: " << '\n' << line_for_save;
						fprintf(ptr, "%s", line_for_save.c_str());
						fclose(ptr);

						//			

						struct tm* tm = localtime(&time_);
						sprintf(log_, "post semi failed with rescode %d at %s", rescode, asctime(tm));
						log_reader("post attendance", log_);

						log_temp.close();
						printf("line  semi remove: %d \n", line_remove);
						delete_line(log_atten_temp, line_remove);
						line_remove = 0;
						log_temp.open(log_atten_temp);
					}
					line_for_save.clear();
				}
				else {
					body.append("\"},");
				}
			}
		}
		remove(log_atten_temp);
	}
}