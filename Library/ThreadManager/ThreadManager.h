#ifndef _THREADMANAGER_H_
#define _THREADMANAGER_H_
//System include
#include <iostream>
#include <cstdint>
#include <cstdio>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>


#include "attendanceService.h"


//In Project include
#include <ultils.h>
#define RELAY   6
constexpr auto MAXTHREAD = 10;
void ThreadManagedInit();
#endif // !_THREADMANAGED_H_
