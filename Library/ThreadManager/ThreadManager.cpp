#include "ThreadManager.h"

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <thread>
#include "../timeService/timeService.h"

#include "../../K3Machine.h"
#include "softTone.h"
#include "ultils.h"
#include "wiringPi.h"
#include "../lcd/lcdService.h"


#include "WorkerThread.h"
#include <thread>

#include "timeService.h"
#include "../lcd/lcdService.h"
#include "../../gpioService.h"

uint8_t ThreadId[MAXTHREAD];
#define BUZZER  7
#define LED_R   5
#define LED_G   4
#define LED_B   3
/*
 * Define for test lcd 
 */
////
extern float personTemperature;
extern std::map<std::string, gpioService::GPIO> ledMap;
extern gpioService *gpio;
extern timeService k3_timer;
extern lcdService* lcd;
extern timeService main_tick_timer;
uint8_t buzzer_en = 0;
extern configMachine config;






void buzzer_function()
{
	while (true)
	{
		if (1 == buzzer_en)
		{
			softToneCreate(BUZZER);
			softToneWrite(BUZZER, 2700);
			msleep(300);
			softToneStop(BUZZER);

			buzzer_en = 0;

		}
		else if (2 == buzzer_en)
		{
			softToneCreate(BUZZER);
			softToneWrite(BUZZER, 2700);
			msleep(200);
			softToneStop(BUZZER);

			msleep(200);
			softToneCreate(BUZZER);
			softToneWrite(BUZZER, 2700);
			msleep(200);
			softToneStop(BUZZER);

			buzzer_en = 0;
		}
		msleep(200);
	}
}

void led_init()
{
	pinMode(RELAY, OUTPUT);
	pinMode(LED_R, OUTPUT);
	pinMode(LED_G, OUTPUT);
	digitalWrite(LED_R, HIGH);
	digitalWrite(LED_G, HIGH);
}


void K3_Thread()
{
	const std::string k3port = "/dev/ttyUSB0";
	K3Machine* K3 = K3Machine::getInstance(k3port);

	K3->open();
	std::string k3_payload;
	std::string tBodyEndline = "cfg.mode";
	int k3char;
	while (true)
	{
		//TODO:

		k3char = K3->read();
		//For Process
		
		if (0xffffffff == k3char) {
			fflush(stdout);
			continue;
		}
		putchar(k3char);
		k3_payload.append(1, static_cast<char>(k3char));

		
		const int index_endf = k3_payload.find("cfg.mode");
		if (index_endf > 0)
		{
			const int filter = k3_payload.find("out of table range");
			//Find "weak low"
			if (filter != -1) {
				std::cout << "Object temperature too low/high -> Skip !!\r";
				buzzer_en = 1;
				lcd->lcd_display("", lcdService::K3_REMIND, lcdService::STATIC_, config.mK3_Config.remind_timeout);
				k3_payload.clear();
				continue;
			}
			

			int index_tbody = k3_payload.find("ambience compensate");
			const int index_weak_low = k3_payload.find("weak low");
			const int index_weak_high = k3_payload.find("weak high");

			if (index_tbody > 0 && index_weak_low > 0)
			{
				k3_payload = k3_payload.substr(index_tbody, index_weak_low - index_tbody);
				index_tbody = k3_payload.find("T body");
			}
			else if (index_tbody > 0 && index_weak_high > 0)
			{
				k3_payload = k3_payload.substr(index_tbody, index_weak_high - index_tbody);
				index_tbody = k3_payload.find("T body");
			}

			k3_payload = k3_payload.substr(index_tbody + 9, 6);
			std::cout << "\r T cut: " << k3_payload << '\r';
			float fBody_measure = std::stof(k3_payload);

			switch (config.device.nTempSafetyLevel)
			{
			case 0:
			{
				personTemperature = fBody_measure;
				std::cout << "Moi ban tap the \r";
				config.mK3_Config.k3_measure_status = TEMP_MEASURED;
				config.mK3_Config.k3_TempSafety = NO_WARNING;

				buzzer_en = 1;
				lcd->lcd_display("Approve", lcdService::K3_APPROVE, lcdService::STATIC_, config.mK3_Config.default_timeout);
				gpio->gpio_control(ledMap[config.device_msg_.msg_default.led.c_str()], gpioService::BLINKY, config.mK3_Config.default_timeout);

				//config.mK3_Config.k3
				k3_timer.stop_timer();
				k3_timer.start_timer_once(config.mK3_Config.default_timeout / 1000);
				break;
			}
			case 1:
			{
				if (fBody_measure <= config.mK3_Config.AlarmUpperLimit)
				{
					personTemperature = fBody_measure;
					std::cout << "Moi ban tap the \r";
					config.mK3_Config.k3_measure_status = TEMP_MEASURED;
					config.mK3_Config.k3_TempSafety = NO_WARNING;

					buzzer_en = 1;
					lcd->lcd_display("Approve", lcdService::K3_APPROVE, lcdService::STATIC_, config.mK3_Config.default_timeout);
					gpio->gpio_control(ledMap[config.device_msg_.msg_default.led.c_str()], gpioService::BLINKY, config.mK3_Config.default_timeout);

					//config.mK3_Config.k3
					k3_timer.stop_timer();
					k3_timer.start_timer_once(config.mK3_Config.default_timeout / 1000);
				}
				else
				{
					//std::cout << "Moi ban lien he trung tam y te\r";
					lcd->lcd_display("Approve", lcdService::K3_REJECT, lcdService::STATIC_, config.mK3_Config.reject_timeout);
					gpio->gpio_control(ledMap[config.device_msg_.msg_tag_failed.led.c_str()], gpioService::TURN_ON, config.mK3_Config.reject_timeout);
					config.mK3_Config.k3_measure_status = TEMP_MEASURED;
					config.mK3_Config.k3_TempSafety = HIGH_SAFETY;

					//config.mK3_Config.k3
					buzzer_en = 2;
					k3_timer.stop_timer();
					k3_timer.start_timer_once(config.mK3_Config.reject_timeout / 1000);
				}
				break;
			}
			}
			
			k3_payload.clear();
		}
		fflush(stdout);
	}
}

void ThreadManagedInit()
{
	//led_init();
	lcd = new lcdService("LcdThread");
	gpio = new gpioService(LED_R, LED_G, LED_B, BUZZER, 2700);
	boost::thread{post_attendace};
	boost::thread{buzzer_function};
	if (config.device.bEnaSemiBoarding) boost::thread{post_semiboarding};
	if (config.device.bEnaTempSafety)
	{
		boost::thread{ K3_Thread };
	}
	
	buzzer_en = 1;

}
