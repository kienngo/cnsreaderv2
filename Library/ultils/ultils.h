#ifndef _ULTILS_H_
#define _ULTILS_H_


#include <iostream>
#include <iosfwd>
#include <regex>
#include <unistd.h>
#include <bits/stdc++.h> 
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <mutex>
#include <unistd.h>
#include <stdint.h>
#include <cstdint>
#include <string.h>
#include <stdio.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <getopt.h>
#include <cerrno>
#include <fcntl.h>
#include <boost/filesystem.hpp>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/reader.h>
#include <chrono>
#include <thread>




#define msleep(x) std::this_thread::sleep_for(std::chrono::milliseconds(x))
//#define msleep(x) std::this_thread::sleep_for (x)



typedef struct {
	std::string eventId;
	bool accessControlOnly;
} eventInformation_t;

typedef struct {
	bool valid;  //dedicated cardnumber in the event
	bool accessControlOnly; //have accessControlOnly
} checkWhitelistCode_t;
typedef enum
{
	FAILED = 0,
	SUCCESS
} support_code;

uint8_t copy_f2f(const char *source, const char *dest);
void log_reader(const char* tag,const char* log);
void delete_line(const char* file_name, int n);
void copy_contents_file(const char* source, const char* dest);
	
	
	
bool write_log_attendance(std::string cardnumber, char* time_write, bool semiboarding, std::string Temperature);
	
	
checkWhitelistCode_t checkByEvent(std::vector<eventInformation_t> eventId, std::string card_number);
bool FileExists(const char* filename);
void get_time(tm** tm);
uint8_t time_check(std::string Time, std::string startTime, std::string stopTime);

std::wstring RemoveVietnameseTone(const std::wstring& text);
Json::Value getJsonContens(std::string fileDir);
std::vector<eventInformation_t> FindEvent(std::string Times);
#endif