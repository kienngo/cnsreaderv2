﻿
#include "ultils.h"


#include "mfrc522cpp.h"
#include "Desfire.h"
#include <vector>


//
//
//

using namespace Json;
using namespace std;
extern std::mutex attendance_mutex;
extern std::mutex semi_mutex;
std::string root_whitelist_dir = "/home/CnsReader/whitelist/";
std::string root_eventId_dir = "/home/CnsReader/event/";
/**********************************************************************************************//**
 * \fn	void log_reader(const char* tag, const char* log)
 *
 * \brief	Logs a reader
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	tag	The tag.
 * \param 	log	The log.
 **************************************************************************************************/

void log_reader(const char* tag, const char* log)
{
	//uint8_t res;
	//time_t time_ = time(nullptr);
	//struct tm* tm = localtime(&time_);
	//FILE* log_running = fopen("/home/CnsReader/log/log_running.txt", "a+");
	//if (nullptr == log)
	//{
	//	printf("failed open log \n");

	//}
	//fprintf(log_running, "%s: %s \n", tag, log);
	//fclose(log_running);
}
//************************************
// Method:    lenth_of_contents
// FullName:  lenth_of_contents
// Access:    public 
// Returns:   uint8_t
// Qualifier:
// Parameter: const char * fileDir
//************************************
uint8_t lenth_of_contents(const char* fileDir) 
{
	std::fstream fFileContents(fileDir);
	if (!fFileContents.is_open())
	{
		return 0;
	}

	fFileContents.seekg(0, std::ios_base::end);
	auto length = fFileContents.tellg();
	fFileContents.seekg(0, std::ios_base::beg);

	fFileContents.close();

	return length;
}
uint8_t copy_f2f(const char *source, const char *dest)
{
	int fdSource = open(source, O_RDWR);

   /* Caf's comment about race condition... */
   if (fdSource > 0){
     if (lockf(fdSource, F_LOCK, 0) == -1) return 0; /* FAILURE */
   }
	else return FAILED; /* FAILURE */

   /* Now the fdSource is locked */

   int fdDest = open(dest, O_CREAT);
   off_t lCount;
   struct stat sourceStat;
   if (fdSource > 0 && fdDest > 0){
      if (!stat(source, &sourceStat)){
          int len = sendfile(fdDest, fdSource, &lCount, sourceStat.st_size);
          if (len > 0 && len == sourceStat.st_size){
               close(fdDest);
               close(fdSource);

               /* Sanity Check for Lock, if this is locked -1 is returned! */
               if (lockf(fdSource, F_TEST, 0) == 0){
                   if (lockf(fdSource, F_ULOCK, 0) == -1){
                      /* WHOOPS! WTF! FAILURE TO UNLOCK! */
                   }
				   else{
                      return SUCCESS; /* Success */
                   }
               }else{
                   /* WHOOPS! WTF! TEST LOCK IS -1 WTF! */
                   return FAILED; /* FAILURE */
               }
          }
      }
   }
   return 0; /* Failure */
}


/**********************************************************************************************//**
 * \fn	void delete_line(const char *file_name, int n)
 *
 * \brief	Deletes the line
 *
 * \author	Zeder
 * \date	6/29/2020
 * \Only for test
 * \param 	file_name	Filename of the file.
 * \param 	n		 	An int to process.
 **************************************************************************************************/
void delete_line(const char* file_name, int n)
{
	// open file in read mode or in mode 
	std::ifstream is(file_name);

	// open file in write mode or out mode 
	std::ofstream ofs;
	//
	//Only for test
	//
	ofs.open("/root/kiin/temp.txt", std::ofstream::out);

	// loop getting single characters 
	char c;
	int line_no = 0;
	auto line = std::string{};
	while (std::getline(is, line))
	{
		// if a newline character 
		if (!line.empty())
		{
			line_no++;
			if (line_no >= n + 1)
				ofs << line << '\n';
		}
		// file content not to be deleted	
	}

	// closing output file 
	ofs.close();

	// closing input file 
	is.close();

	// remove the original file 
	remove(file_name);

	// rename the file 
	rename("/root/kiin/temp.txt", file_name);
}

/**********************************************************************************************//**
 * \fn	void copy_contents_file(const char *source, const char *dest)
 *
 * \brief	Copies the contents file
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param 	source	Another instance to copy.
 * \param 	dest  	Destination for the.
 **************************************************************************************************/

void copy_contents_file(const char* source, const char* dest)
{
	// open file in read mode or in mode 
	std::ifstream is(source);

	// open file in write mode or out mode 
	std::ofstream ofs;
	ofs.open(dest, std::ofstream::app);

	auto line = std::string{ };
	while (std::getline(is, line))
	{
		// if a newline character 
		if (!line.empty())
		{
			ofs << line << '\n';
		}
		// file content not to be deleted	
	}

	// closing output file 
	ofs.close();
	// closing input file 
	is.close();

}

/**********************************************************************************************//**
 * \fn	std::wstring RemoveVietnameseTone(const std::wstring& text)
 *
 * \brief	Removes the Vietnamese tone described by text
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param 	text	The text.
 *
 * \returns	A std::wstring.
 **************************************************************************************************/

std::wstring RemoveVietnameseTone(const std::wstring& text) {
	std::wstring result(text);
	result = std::regex_replace(result, std::wregex(L"à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|/g"), L"a");
	result = std::regex_replace(result, std::wregex(L"À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|/g"), L"A");
	result = std::regex_replace(result, std::wregex(L"è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|/g"), L"e");
	result = std::regex_replace(result, std::wregex(L"È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|/g"), L"E");
	result = std::regex_replace(result, std::wregex(L"ì|í|ị|ỉ|ĩ|/g"), L"i");
	result = std::regex_replace(result, std::wregex(L"Ì|Í|Ị|Ỉ|Ĩ|/g"), L"I");
	result = std::regex_replace(result, std::wregex(L"ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|/g"), L"o");
	result = std::regex_replace(result, std::wregex(L"Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|/g"), L"O");
	result = std::regex_replace(result, std::wregex(L"ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|/g"), L"u");
	result = std::regex_replace(result, std::wregex(L"Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|/g"), L"U");
	result = std::regex_replace(result, std::wregex(L"ỳ|ý|ỵ|ỷ|ỹ|/g"), L"y");
	result = std::regex_replace(result, std::wregex(L"Ỳ|Ý|Ỵ|Ỷ|Ỹ|/g"), L"y");
	result = std::regex_replace(result, std::wregex(L"đ"), L"d");
	result = std::regex_replace(result, std::wregex(L"Đ"), L"D");
	return result;
}

void get_time(tm** tm)
{
	time_t time_ = time(nullptr);
	*tm = localtime(&time_);
}


bool write_log_attendance(std::string cardnumber, char* time_write, bool semiboarding,std::string Temperature) {

	
	FILE* log_attendance;

	//
	if(semiboarding) {
//		std::lock_guard<std::mutex> guard(semi_mutex);
		log_attendance = fopen("/home/CnsReader/attendance/SemiBoarding.txt", "a+");
		fprintf(log_attendance,
			"%s,%s\n",
			cardnumber.c_str(),
			time_write);
	}
	else {
		
//		std::lock_guard<std::mutex> guard(attendance_mutex);
		
		log_attendance = fopen("/home/CnsReader/attendance/logattendance.txt", "a+");
		fprintf(log_attendance,
			"%s,%s,%s\n",
			cardnumber.c_str(),
			time_write,
			Temperature.c_str());
	}
	if (nullptr == log_attendance)
	{
		printf("failed to open log file \n");
		return false;
	}


	
	

	fclose(log_attendance);

	return true;

}

checkWhitelistCode_t checkByEvent(std::vector<eventInformation_t> eventId, 
	std::string card_number)
{
	checkWhitelistCode_t checkCode = {false,false};	
	std::string whitelist_dir, whitelist_file;
	uint8_t result;
	std::fstream fs;
	Json::Value whitelistFileContents;
	struct tm* tm;
	get_time(&tm);
	char time_create[50];
	sprintf(time_create, "%04d%02d%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday);
	
	
	whitelist_dir.clear();
	whitelist_dir.append(root_whitelist_dir);
	//	whitelist_dir.append(std::string(time_create));
    whitelist_file.clear();
	whitelist_file.append(whitelist_dir);
	whitelist_file.append(card_number);
	whitelist_file.append(".txt");
	std::cout << "check card number file: " << whitelist_file << '\n';
	
	whitelistFileContents = getJsonContens(whitelist_file);
	if (whitelistFileContents.empty())
	{
		std::cout << "card number not in whitelist: " << whitelist_file << '\n';
		return checkCode;
	}
//	std::cout << "whitelistFileContents" << whitelistFileContents.asString() << '\n';
	uint8_t count = 0;
	std::cout << "Begin search and compare eventId in cardnumber file " << '\n';
	for (eventInformation_t eventIDcheck : eventId)
	{
		std::cout << eventIDcheck.eventId << '\n';
		for (int i = 0; i < whitelistFileContents["eventid"].size(); i++)
		{
			//
			//if eventIDcheck.eventId  \\Event in eventId.txt 
			// == whitelistFileContents["eventid"][i].asString()  \\Event in card number file 
			// => check accessControlOnly and return 
			if (eventIDcheck.eventId == whitelistFileContents["eventid"][i].asString())
			{
				
				//
				//
				//
				checkCode.valid = true;
				if (eventIDcheck.accessControlOnly) checkCode.accessControlOnly = eventIDcheck.accessControlOnly;
				
			}
		}
	}
	
	return checkCode;
	
}
Json::Value getJsonContens(std::string fileDir)
{
	
	
	Json::Reader reader;
	Json::Value jsonValue;
	std::fstream fs_file;
//	std::string fs;
	if (FileExists(fileDir.c_str()))
	{
		fs_file.open(fileDir.c_str(), std::fstream::in); 
		if (!fs_file.is_open())
		{
			cout << "Get json contents failed  \n";
		}

		fs_file >> jsonValue;
		
//		std::string jsonValueS = jsonValue.get("encoding", "UTF-8").asString(); 
//		cout << "get jSonValue: done" << '\n';
		
		
		fs_file.close();
		
		
	}
	
	
	return jsonValue;
}
bool FileExists(const char* filename) 
{
	struct stat fileInfo;
	return stat(filename, &fileInfo) == 0;
}

std::vector<eventInformation_t> FindEvent(std::string Times)
{
	std::vector<eventInformation_t> result;
	std::fstream fs;
	std::string eventId_dir;
	eventId_dir.append(root_eventId_dir);
	eventId_dir.append("event");
	eventId_dir.append(".txt");
	
	std::cout << "eventId_dir: " << eventId_dir << std::endl;
	
	eventInformation_t eventInformation;
//	std::string eventID;
//	bool accessControlOnly;
	std::string event_start_time;
	std::string event_stop_time;
	if (FileExists(eventId_dir.c_str()))
	{
	
		Json::Value eventId_value;
		eventId_value = getJsonContens(eventId_dir);
//		std::cout << eventId_value.asString() << std::endl;
		
		if (eventId_value.isNull())
		{ 
			return vector<eventInformation_t>();
		}
		
		
		int numofEvent = eventId_value["events"].size();
		for (int i = 0; i < numofEvent; i++)
		{
			
			//
			//
			//
			eventInformation.eventId = eventId_value["events"][i]["id"].asString();
			eventInformation.accessControlOnly = eventId_value["events"][i]["accessControlOnly"].asBool();
			
			event_start_time =  eventId_value["events"][i]["time"]["startDateTime"].asString();
			event_stop_time  =  eventId_value["events"][i]["time"]["endDateTime"].asString();
			
		
			//
			//
			//
			event_start_time = event_start_time.substr(event_start_time.find_last_of(" "), event_start_time.size());
			event_stop_time = event_stop_time.substr(event_stop_time.find_last_of(" "), event_stop_time.size());
			cout << "eventId " << eventInformation.eventId << '\n';
			cout <<	"event_start_time : "  << event_start_time;
			cout << " -  event_stop_time:  " << event_stop_time << '\n'; 
			//
			//
			//
			if (time_check(Times, event_start_time, event_stop_time))
			{
				cout << "Intime - Event Id: " <<  eventInformation.eventId << endl;
				result.push_back(eventInformation);
			}
		}
	}
	return result;
}

uint8_t time_check(std::string timeFindEvent, std::string startTime, std::string stopTime)
{
	int nTimeFindEvent_Hours, nTimeFindEvent_Min;
	int startHour, startMin;
	int stopHour, stopMin;
	stringstream    sTimeFindEvent; sTimeFindEvent << timeFindEvent;
	stringstream	sStartTime; sStartTime << startTime;
	stringstream	sStopTime; sStopTime  << stopTime;
	
	sTimeFindEvent >> nTimeFindEvent_Hours;
	sTimeFindEvent.get();
	sTimeFindEvent >> nTimeFindEvent_Min;
	
	sStartTime >> startHour;
	sStartTime.get();
	sStartTime >> startMin;
	
	
	sStopTime  >> stopHour;
	sStopTime.get();
	sStopTime >> stopMin;
	
	
	long startSecond = startHour * 60 * 60 + startMin * 60;
	long stopSecond = stopHour * 60 * 60 + stopMin * 60;
	long nTimeFindEvent_Second  = nTimeFindEvent_Hours * 60 * 60 + nTimeFindEvent_Min * 60;
	
//	if (startHour == stopHour)
//	{
//		if (!((nTimeFindEvent_Min >= startMin) && (nTimeFindEvent_Min <= stopMin)))	return 0;
//	}
//	else if (!((nTimeFindEvent_Hours >= startHour) && (nTimeFindEvent_Hours <= stopHour))) return 0;
	
	if (nTimeFindEvent_Second <= startSecond || nTimeFindEvent_Second >= stopSecond) return 0;
	return 1;
	
}