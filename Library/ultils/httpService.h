#ifndef _HTTPSERVICE_H
#define _HTTPSERVICE_H
#include <fcntl.h>
#include <getopt.h>
#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <linux/types.h>
#include <sys/ioctl.h>
#include "time.h"


struct MemoryStruct {
	char* memory;
	size_t size;
	uint16_t res_code;
}
;


#ifdef __cplusplus
extern "C" {
#endif


	typedef uint8_t code;


	uint16_t device_alive(char* deviceID);
	MemoryStruct get_whitelist(char* deviceID);

	MemoryStruct get_whitelist_v2(char* deviceID, bool today);
	uint16_t post_attendance(char* body);
	uint16_t post_attendance_semiboarding(char* body);

	//MCP API v2
	MemoryStruct device_alive_ver2(char* deviceID);
	uint16_t delete_notification(const char* deviceID, char* date);


#ifdef __cplusplus
}
#endif

#endif
