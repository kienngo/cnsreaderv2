
#include "httpService.h"
#include "configMachine.hpp"
#include "ultils.h"
#include <iostream>
using namespace std;
#define UAT
#ifdef UAT

#define ENDL '\n'

//UAT
#define URL_WHITELIST_UAT "https://uat-api.vinaid.vn:1443/api/attendance/white-list/"
#define URL_KEEP_ALIVE_UAT "https://uat-api.vinaid.vn/api/terminalState"
#define URL_POST_ATTENDANCE "https://uat-api.vinaid.vn:1443/api/attendance/bulk-upload"
#define TOKEN_WHITELIST_UAT "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkZXZpY2VAMTIiLCJ0ZW5hbnRJZCI6IjEyIiwidGVybWluYWxJZCI6IjAwMTAwMDAxIiwiZXhwIjoxNjIxMDA4Mzg0fQ.2EFK87usz1CRTotrMqKPD_9fGWcA9chrbNHB69ExB-7p7ROXawvW7m9rc0Wn_7N6pw5zYaJzKj1YSH_j6kuCMA"
#define TOKEN_KEEPALIVE_UAT "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJvbmxpbmUiLCJ0ZW5hbnRJZCI6IjEyIiwidGVybWluYWxJZCI6IjAwMTAwMDAxIiwiZXhwIjoxNjIxMDA4Mzg0fQ.45Y1LBvZBWjwI5MswuzKzg5QqK0zRN4UsZsFJO9vuEY8wzQUS7qbVjd9bT8xa3c7WkuBbbmsfc5o4UOB-2j9ww"

//#define URL_WHITELIST_UAT	  "https://api.vinaid.vn:1443/api/attendance/white-list/"
//#define URL_KEEP_ALIVE_UAT "https://api.vinaid.vn/api/terminalState"
//#define URL_POST_ATTENDANCE "https://api.vinaid.vn:1443/api/attendance/bulk-upload"
//#define TOKEN_WHITELIST_UAT  "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkZXZpY2VAMiIsImV4cCI6MTYwNTEwNTI2Nn0.rGItTSxaHmM4_6X5UdV5XcslUrnnCWhBQieiWW7scnSKu8dfOU7brOJvk4DMDPrfcgTZLovugawlLontj6fKZA"
//#define TOKEN_KEEPALIVE_UAT  "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsInRlbmFudElkIjoiMiIsImV4cCI6MTYwNTA5OTc4NX0.g1Ys1o8p7pIhxgh3ver_Re5LDEKPnkf9haqENAc7Vuqn4g2p3u-KeII9fyDLfkJAQa1hA3xU-9b23qMalcN-NQ"


#endif
extern configMachine config;

/**********************************************************************************************//**
 * \fn	size_t WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp)
 *
 * \brief	Writes a memory callback
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param [in,out]	contents	If non-null, the contents.
 * \param 		  	size		The size.
 * \param 		  	nmemb   	The nmemb.
 * \param [in,out]	userp   	If non-null, the userp.
 *
 * \returns	A size_t.
 **************************************************************************************************/

size_t WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp)
{

	size_t realsize = size * nmemb;
	struct MemoryStruct* mem = (struct MemoryStruct*)userp;

	char* ptr = (char*)realloc(mem->memory, mem->size + realsize + 1);
	if (!ptr) {
		/* out of memory! */
		printf("not enough memory (realloc returned NULL)\n");
		return 0;
	}

	mem->memory = ptr;
	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}

/**********************************************************************************************//**
 * \fn	uint8_t device_alive(char* deviceID)
 *
 * \brief	Device alive
 *
 * \author	Zeder
 * \date	6/23/2020
 *
 * \param [in,out]	deviceID	If non-null, identifier for the device.
 *
 * \returns	An uint8_t.
 **************************************************************************************************/

uint16_t device_alive(char* deviceID)
{

	printf("keep alive \n");
	CURL* curl;
	CURLcode res;
	curl = curl_easy_init();
	uint16_t response_code = 0;
	char data[100];
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(curl, CURLOPT_URL, config.sKeepAliveURL.c_str());
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");
		struct curl_slist* headers = nullptr;
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, config.sTokenKeepAlive.c_str());
		headers = curl_slist_append(headers, "Content-Type: text/plain");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		sprintf(data, "{\"interval\":60,\"terminalId\":\"%s\",\"version\":143}", config.sDeviceID.c_str());
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
		res = curl_easy_perform(curl);


		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
		printf("\n");
	}
	curl_easy_cleanup(curl);
	return response_code;
}
/**********************************************************************************************//**
 * \fn	uint8_t device_alive_ver2(char* deviceID)
 *
 * \brief	Device alive
 *
 * \author	Zeder
 * \date	8/24/2020
 *
 * \param [in,out]	deviceID	If non-null, identifier for the device.
 *
 * \returns	An uint8_t.
 **************************************************************************************************/
MemoryStruct device_alive_ver2(char* deviceID)
{
	printf("keep alive api v2\n");
	CURL* curl;
	CURLcode res;
	curl = curl_easy_init();

	struct MemoryStruct mServerResponse;
	mServerResponse.memory = (char*)malloc(1);
	mServerResponse.size = 0;
	char data[100];
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		//		curl_easy_setopt(curl, CURLOPT_URL, config.sKeepAliveURL.c_str());
//		curl_easy_setopt(curl, CURLOPT_URL, "https://uat-api.vinaid.vn/api/terminalState/v2");
		curl_easy_setopt(curl, CURLOPT_URL, config.sKeepAliveURL.c_str());
		
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");
		struct curl_slist* headers = nullptr;
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, config.sTokenKeepAlive.c_str());
		headers = curl_slist_append(headers, "Content-Type: text/plain");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		sprintf(data, "{\"interval\":60,\"terminalId\":\"%s\",\"version\":143}", config.sDeviceID.c_str());
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&mServerResponse);
		res = curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &mServerResponse.res_code);
		printf("\n");
	}
	curl_easy_cleanup(curl);
	return mServerResponse;
}
uint16_t delete_notification(const char* deviceID, char* date)
{
	CURL* curl;
	CURLcode res;
	curl = curl_easy_init();
	
	struct MemoryStruct mServerResponse;
	mServerResponse.memory = (char*)malloc(1);
	mServerResponse.size = 0;
	char data[100];
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_easy_setopt(curl, CURLOPT_URL, config.sDeleteNotificationURL.c_str());
		//		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
				curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");
		struct curl_slist *headers = NULL;
		headers = curl_slist_append(headers, config.sTokenKeepAlive.c_str());
		headers = curl_slist_append(headers, "Content-Type: application/json");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		sprintf(data, "{\"terminalId\":\"%s\",\"toDate\": \"%s\"}", deviceID, date);
		std::cout << "notifi delete " << std::endl << data << '\n';
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
		
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&mServerResponse);
		res = curl_easy_perform(curl);
	
		
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &mServerResponse.res_code);
		printf("\n");
	}
	curl_easy_cleanup(curl);
	return mServerResponse.res_code;
}
/**********************************************************************************************//**
 * \fn	MemoryStruct get_whitelist(char* deviceID)
 *
 * \brief	Gets a white list
 *
 * \author	Zeder
 * \date	6/23/2020
 *
 * \param [in,out]	deviceID	If non-null, identifier for the device.
 *
 * \returns	The whitelist.
 **************************************************************************************************/

MemoryStruct get_whitelist(char* deviceID)
{
	CURL* curl;
	CURLcode res;
	uint8_t response_code = 0;

	struct MemoryStruct _whitelist;
	_whitelist.memory = (char*)malloc(1);
	_whitelist.size = 0;

	char url[100];
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
		sprintf(url, "%s%s", config.sWhiteListURL.c_str(), config.sDeviceID.c_str());

		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");
		struct curl_slist* headers = nullptr;
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, config.sTokenWhiteList.c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&_whitelist);

		res = curl_easy_perform(curl);

		uint16_t response_code;
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
		_whitelist.res_code = response_code;
		//fprintf(stderr, "\n%s\n", _whitelist.memory);
	}
	curl_easy_cleanup(curl);
	return _whitelist;
}

/**********************************************************************************************//**
 * \fn	MemoryStruct get_whitelist(char* deviceID)
 *
 * \brief	Gets a white list
 *
 * \author	Zeder
 * \date	6/23/2020
 *
 * \param [in,out]	deviceID	If non-null, identifier for the device.
 *
 * \returns	The white list.
 **************************************************************************************************/

MemoryStruct get_whitelist_v2(char* deviceID, bool today)
{
	CURL* curl;
	CURLcode res;
	uint8_t response_code = 0;

	struct MemoryStruct mServerResponse;
	mServerResponse.memory = (char*)malloc(1);
	mServerResponse.size = 0;

	struct tm* tm;
	get_time(&tm);

	char url[100];

	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
		
		if (today)
		{
			sprintf(url, "%s?deviceNo=%s&eventDate=%04d-%02d-%02d", config.sWhiteListURL.c_str(), config.sDeviceID.c_str(), tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday);
		}
		else
		{
			//Tommorrow
			sprintf(url, "%s?deviceNo=%s&eventDate=%04d-%02d-%02d", config.sWhiteListURL.c_str(), config.sDeviceID.c_str(), tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday + 1);
		}
		
		std::cout << url << '\n';
		curl_easy_setopt(curl, CURLOPT_URL, url);
		//		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
				//HEADER
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");

		struct curl_slist* headers = nullptr;
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, config.sTokenWhiteList.c_str());

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

		//FUNCTION Callback
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&mServerResponse);

		//Perform
		res = curl_easy_perform(curl);

		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &mServerResponse.res_code);
		//		fprintf(stderr, "\n%s\n", mServerResponse.memory);
	}
	curl_easy_cleanup(curl);

	return mServerResponse;
	
}
/**********************************************************************************************//**
 * \fn	uint16_t post_attendance(char* body)
 *
 * \brief	Posts an attendance
 *
 * \author	Zeder
 * \date	6/23/2020
 *
 * \param [in,out]	body	If non-null, the body.
 *
 * \returns	An uint16_t.
 **************************************************************************************************/

uint16_t post_attendance(char* body)
{
	CURL* curl;
	CURLcode res;


	char   buf[80];
	time_t time_ = time(nullptr);

	struct tm* tm = localtime(&time_);

	sprintf(buf, "%04d%02d%02d%02d%02d%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);


	char body_templates[20000];

	uint16_t rescode;
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(curl, CURLOPT_URL, config.url_.url_attendance.c_str());
		//curl_easy_setopt(curl, CURLOPT_URL, config.sPostAttendanceURL.c_str());
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");
		struct curl_slist* headers = nullptr;
		headers = curl_slist_append(headers, "Content-Type: application/json");
		std::string authorization = std::string("Authorization: ");
		authorization.append(config.config_.mcpConfig_.attendanceToken);
		headers = curl_slist_append(headers, authorization.c_str());
		headers = curl_slist_append(headers, "Content-Type: application/json");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		sprintf(body_templates, "{\"data\": [%s]}", body);
		std::cout << "body " << body_templates << "\n" << '\n';
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body_templates);

		res = curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &rescode);
		cout << ENDL << rescode << ENDL;
		//		printf("\n");
	}
	curl_easy_cleanup(curl);
	return rescode;
}

uint16_t post_attendance_semiboarding(char* body)
{
	CURL* curl;
	CURLcode res;


	char   buf[80];
	time_t time_ = time(nullptr);

	struct tm* tm = localtime(&time_);

	sprintf(buf, "%04d%02d%02d%02d%02d%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);


	char body_templates[20000];

	uint16_t rescode;
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		curl_easy_setopt(curl, CURLOPT_URL, config.url_.url_semi_boarding.c_str());
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");
		struct curl_slist* headers = nullptr;
		headers = curl_slist_append(headers, "Content-Type: application/json");
		std::string authorization = std::string("Authorization: ");
		authorization.append(config.config_.mcpConfig_.attendanceToken);
		headers = curl_slist_append(headers, authorization.c_str());
		headers = curl_slist_append(headers, "Content-Type: application/json");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		sprintf(body_templates, "{\"data\": [%s]}", body);
		std::cout << "body semi" << body_templates << "\n" << '\n';
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body_templates);

		res = curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &rescode);
	}

	curl_easy_cleanup(curl);

	return rescode;
}
