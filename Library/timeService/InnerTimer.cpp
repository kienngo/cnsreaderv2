﻿#include "InnerTimer.h"
#include <boost/asio.hpp>
#include <boost/bind.hpp>


InnerTimer::InnerTimer(size_t time, const std::function<void()>& functionCallback): time_{ boost::posix_time::millisec{time}}, InnerFunction{functionCallback}
{
}

InnerTimer::~InnerTimer()
{
	wait_thread.join();
}

void InnerTimer::wait_then_call()
{
	std::unique_lock<std::mutex> lck{mtx};
	
	std::cout << "Thread " << wait_thread.get_id() << '\n';
	cv.wait_for(lck, time);
	
	InnerFunction();
}
void InnerTimer::wait_deadline_timer()
{
	boost::asio::io_context service;
	boost::asio::deadline_timer timer(service, time_);
	std::cout << "Thread " << wait_thread.get_id() << '\n';
	timer.async_wait(boost::bind(this->InnerFunction));
	service.run();
}
