#include "timeService.h"
#include "Library/lcd/lcdService.h"




//extern lcdService lcd;
//extern configMachine config;

timeService::timeService(CallbackFunc m_handlerCallback)
{
	this->handlerCallback = m_handlerCallback;
	
}

	
void timeService::createTimer(char* name, 
	int intervalMS)
{
	
	struct sigevent         te {};
	struct sigaction        sa {};
	//	int                     sigNo = SIGRTMIN;
	int						sigNo = SIGUSR1;
	// Set up signal handler.
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = timeService::handlerCallback;
	sigemptyset(&sa.sa_mask);

	if (sigaction(sigNo, &sa, NULL) == -1)
		errExit("sigaction");

	// Set up timer
	te.sigev_notify = SIGEV_SIGNAL;
//	te.sigev_notify = SIGEV_NONE;
	te.sigev_signo = sigNo;
	te.sigev_value.sival_ptr = name;

	//timer_t timerID;

	if(timer_create(CLOCK_REALTIME, &te, &this -> timerID) == -1)
		errExit("timer_create");
	
}
void timeService::createTimer(char* name, 
	int intervalMS, int sigNo)
{

	struct sigevent         te {};
	struct sigaction        sa {};
	//	int                     sigNo = SIGRTMIN;
	int						m_sigNo = sigNo;
	// Set up signal handler.
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = timeService::handlerCallback;
	sigemptyset(&sa.sa_mask);

	if (sigaction(sigNo, &sa, NULL) == -1)
		errExit("sigaction");

	// Set up timer
	te.sigev_notify = SIGEV_SIGNAL;
	//	te.sigev_notify = SIGEV_NONE;
	te.sigev_signo = m_sigNo;
	te.sigev_value.sival_ptr = name;

	//timer_t timerID;

	if(timer_create(CLOCK_REALTIME, &te, &this->timerID) == -1)
		errExit("timer_create");
	
}
void timeService::start_timer() {
	struct itimerspec       its {};
	its.it_value.tv_sec = 1;
	its.it_value.tv_nsec = 0;
	its.it_interval.tv_sec = 1;
	its.it_interval.tv_nsec = 0;

	
	if (timer_settime(this->timerID, 0, &its, nullptr) == -1)
		errExit("timer_settime");
}

void timeService::start_timer(itimerspec its)
{
	struct itimerspec       m_its {};
	m_its = its;

	this->timerIsEnabled = true;
	if (timer_settime(this->timerID, 0, &m_its, nullptr) == -1)
		errExit("timer_settime");
}

void timeService::start_timer_once(int intervalMS)
{
	struct itimerspec       its {};
	its.it_value.tv_sec = intervalMS;
	its.it_value.tv_nsec = 0;
	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = 0;

	this->timerIsEnabled = true;
	if (timer_settime(this->timerID, 0, &its, nullptr) == -1)
		errExit("timer_settime");
}

void timeService::stop_timer()
{
	itimerspec its;
	its.it_value.tv_sec = 0;
	its.it_value.tv_nsec = 0;
	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = 0;
	timer_settime(this->timerID, 0, &its, NULL);
}



