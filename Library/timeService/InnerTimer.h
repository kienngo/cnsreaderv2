﻿#pragma once
#include <iostream>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <bits/std_function.h>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time_duration.hpp>

class InnerTimer
{
public:
	InnerTimer(size_t time, const std::function<void(void)>& functionCallback);
	~InnerTimer();
    void wait_deadline_timer();
	void wait_then_call();
private:
    
    std::mutex mtx;
    std::condition_variable cv{};
    std::chrono::milliseconds time;
    boost::posix_time::millisec time_;
    std::function <void(void)> InnerFunction;
    std::thread wait_thread{ [this]() { wait_deadline_timer(); } };
};
