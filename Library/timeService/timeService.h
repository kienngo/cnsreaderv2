#ifndef TIMER_SERVICE_H__
#define TIMER_SERVICE_H__

#include <iostream>
#include <sys/time.h>
#include <cstdint>
#include <cstring>
#include <cerrno>
#include <csignal>

#define MSG_SUCCESS "MSG_SUCCESS"
#define DEFAULT_MSG "DEFAULT_MSG"

using CallbackFunc = void(*)(int sig, siginfo_t* si, void* uc);
class timeService
{
public:
	timer_t timerID;
	bool timerIsEnabled = 0;
	CallbackFunc handlerCallback;
	timeService(CallbackFunc handlerCallback);
	void stop_timer();
	void start_timer();
	void start_timer(itimerspec its);
	void start_timer_once(int intervalMS);
	
	void createTimer(char* name, int intervalMS);
	void createTimer(char* name, 
		int intervalMS,int sigNo);
private:
	itimerspec       its {};
	
};

#define errExit(msg) do { perror(msg); exit(EXIT_FAILURE); } while (0)



#endif

