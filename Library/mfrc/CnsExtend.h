#ifndef CNS_MFRC522Extend__
#define CNS_MFRC522Extend__
#include "mfrc522cpp.h"
#include "Desfire.h"


class CnsExtended : public DESFire
{
	

public:
	explicit CnsExtended() :DESFire() {};
	explicit CnsExtended(uint8_t* file_descriptor) : DESFire(file_descriptor) {};
	//CnsExtended(DESFire mfrc);
	~CnsExtended();

	std::string CnsGetName();
	std::string CnsGetCardNumber();
	std::string CnsGetPocket();
	bool StartReadCard();
private:
	DESFire _mfrc_cnsextend;
	std::string Classic_ReadName();
	std::string Classic_ReadData();
	std::string Classic_ReadCardNumber();
	std::string Classic_ReadValue();
	std::string Mifare_ReadCardNumber();
	std::string Mifare_ReadData();
	std::string Mifare_ReadValue();
	std::string Mifare_ReadName();
};


void  Classic_GetDiversify(MFRC522::Uid uid, DESFire::MIFARE_Key* key, uint8_t password[]);

#endif