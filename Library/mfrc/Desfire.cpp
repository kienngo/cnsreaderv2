#include "Desfire.h"
#include <cstring>
MFRC522::StatusCode DESFire::PICC_RequestATS(byte* atsBuffer, byte* atsLength)
{
	MFRC522::StatusCode result;

	// Build command buffer
	atsBuffer[0] = 0xE0; //PICC_CMD_RATS;
	atsBuffer[1] = 0x50; // FSD=64, CID=0

	// Calculate CRC_A
	byte status = PCD_CalculateCRC(atsBuffer, 2, &atsBuffer[2]);
	if (status != STATUS_OK) {
		return result;
	}

	// Transmit the buffer and receive the response, validate CRC_A.
	status = PCD_TransceiveData(atsBuffer, 4, atsBuffer, atsLength, NULL, 0, true);
	if (status != STATUS_OK) {
		PICC_HaltA();
		// printf("WTF???");
		result = MFRC522::STATUS_ERROR;
		return result;
	}
	result = MFRC522::STATUS_OK;
	return result;
} // End PICC_RequestATS()

  /**
  * Transmits Protocol and Parameter Selection Request (PPS)
  *
  * @return STATUS_OK on success, STATUS_??? otherwise.
  */
MFRC522::StatusCode DESFire::PICC_ProtocolAndParameterSelection(byte cid,	///< The lower nibble indicates the CID of the selected PICC in the range of 0x00 and 0x0E
	byte pps0,	///< PPS0
	byte pps1	///< PPS1
) {
	MFRC522::StatusCode result;

	byte ppsBuffer[5];
	byte ppsBufferSize = 5;
	ppsBuffer[0] = 0xD0 | (cid & 0x0F);
	ppsBuffer[1] = pps0;
	ppsBuffer[2] = pps1;

	// Calculate CRC_A
	byte status = PCD_CalculateCRC(ppsBuffer, 3, &ppsBuffer[3]);
	if (status != STATUS_OK) {
		result = STATUS_ERROR;
		return result;
	}

	// Transmit the buffer and receive the response, validate CRC_A.
	status = PCD_TransceiveData(ppsBuffer, 5, ppsBuffer, &ppsBufferSize, NULL, 0, true);
	if (status == STATUS_OK) {
		// This is how my MFRC522 is by default.
		// Reading https://www.nxp.com/documents/data_sheet/MFRC522.pdf it seems CRC generation can only be disabled in this mode.
		if (pps1 == 0x00) {
			PCD_WriteRegister(TxModeReg, 0x00);
			PCD_WriteRegister(RxModeReg, 0x00);
		}
	}
	result = STATUS_OK;
	return result;
} // End PICC_ProtocolAndParameterSelection()

/**
 * @see MIFARE_BlockExchangeWithData()
 */
DESFire::StatusCode DESFire::MIFARE_BlockExchange(mifare_desfire_tag* tag, byte cmd, byte* backData, byte* backLen)
{
	return MIFARE_BlockExchangeWithData(tag, cmd, NULL, NULL, backData, backLen);
} // End MIFARE_BlockExchange()

/**
 *
 * Frame Format for DESFire APDUs
 * ==============================
 *
 * The frame format for DESFire APDUs is based on only the ISO 14443-4 specifications for block formats.
 * This is the format used by the example firmware, and seen in Figure 3.
 *  - PCB – Protocol Control Byte, this byte is used to transfer format information about each PDU block.
 *  - CID – Card Identifier field, this byte is used to identify specific tags. It contains a 4 bit CID value as well
 *          as information on the signal strength between the reader and the tag.
 *  - NAD – Node Address field, the example firmware does not support the use of NAD.
 *  - DESFire Command Code – This is discussed in the next section.
 *  - Data Bytes – This field contains all of the Data Bytes for the command
 *
 *  |-----|-----|-----|---------|------|----------|
 *  | PCB | CID | NAD | Command | Data | Checksum |
 *  |-----|-----|-----|---------|------|----------|
 *
 * Documentation: http://read.pudn.com/downloads64/ebook/225463/M305_DESFireISO14443.pdf
 *                http://www.ti.com.cn/cn/lit/an/sloa213/sloa213.pdf
 */
DESFire::StatusCode DESFire::MIFARE_BlockExchangeWithData(mifare_desfire_tag* tag, byte cmd, byte* sendData, byte* sendLen, byte* backData, byte* backLen)
{
	StatusCode result;

	byte buffer[64];
	byte bufferSize = 64;
	byte sendSize = 3;

	buffer[0] = tag->pcb;
	buffer[1] = tag->cid;
	buffer[2] = cmd;

	// Append data if available
	if (sendData != NULL && sendLen != NULL) {
		if (*sendLen > 0) {
			memcpy(&buffer[3], sendData, *sendLen);
			sendSize = sendSize + *sendLen;
		}
	}

	// Update the PCB
	if (tag->pcb == 0x0A)
		tag->pcb = 0x0B;
	else
		tag->pcb = 0x0A;

	// Calculate CRC_A
	byte status = PCD_CalculateCRC(buffer, sendSize, &buffer[sendSize]);
	if (status != STATUS_OK) {
		result.mfrc522 = STATUS_ERROR;
		return result;
	}

	status = PCD_TransceiveData(buffer, sendSize + 2, buffer, &bufferSize);
	if (status != STATUS_OK) {
		result.mfrc522 = STATUS_ERROR;
		return result;
	}

	// Set the DESFire status code
	result.desfire = (DesfireStatusCode)(buffer[2]);
	result.mfrc522 = STATUS_OK;
	// Copy data to backData and backLen
	if (backData != NULL && backLen != NULL) {
		memcpy(backData, &buffer[3], bufferSize - 5);
		*backLen = bufferSize - 5;
	}

	return result;
} // End MIFARE_BlockExchangeWithData()

DESFire::StatusCode DESFire::MIFARE_DESFIRE_GetVersion(mifare_desfire_tag* tag, MIFARE_DESFIRE_Version_t* versionInfo)
{
	StatusCode result;
	byte versionBuffer[64];
	byte versionBufferSize = 64;

	result = MIFARE_BlockExchange(tag, 0x60, versionBuffer, &versionBufferSize);
	if (result.mfrc522 == STATUS_OK) {
		byte hardwareVersion[2];
		byte storageSize;

		versionInfo->hardware.vendor_id = versionBuffer[0];
		versionInfo->hardware.type = versionBuffer[1];
		versionInfo->hardware.subtype = versionBuffer[2];
		versionInfo->hardware.version_major = versionBuffer[3];
		versionInfo->hardware.version_minor = versionBuffer[4];
		versionInfo->hardware.storage_size = versionBuffer[5];
		versionInfo->hardware.protocol = versionBuffer[6];

		if (result.desfire == MF_ADDITIONAL_FRAME) {
			result = MIFARE_BlockExchange(tag, 0xAF, versionBuffer, &versionBufferSize);
			if (result.mfrc522 == STATUS_OK) {
				versionInfo->software.vendor_id = versionBuffer[0];
				versionInfo->software.type = versionBuffer[1];
				versionInfo->software.subtype = versionBuffer[2];
				versionInfo->software.version_major = versionBuffer[3];
				versionInfo->software.version_minor = versionBuffer[4];
				versionInfo->software.storage_size = versionBuffer[5];
				versionInfo->software.protocol = versionBuffer[6];
			}
			else {
				printf("Failed to send AF: ");
			}

			if (result.desfire == MF_ADDITIONAL_FRAME) {
				byte nad = 0x60;
				result = MIFARE_BlockExchange(tag, 0xAF, versionBuffer, &versionBufferSize);
				if (result.mfrc522 == STATUS_OK) {
					memcpy(versionInfo->uid, &versionBuffer[0], 7);
					memcpy(versionInfo->batch_number, &versionBuffer[7], 5);
					versionInfo->production_week = versionBuffer[12];
					versionInfo->production_year = versionBuffer[13];
				}
				else {
					printf("Failed to send AF: ");
				}
			}

			if (result.desfire == MF_ADDITIONAL_FRAME) {
				printf("GetVersion(): More data???");
			}
		}
	}
	else {
		printf("Version(): Failure.");
	}

	return result;
} // End MIFARE_DESFIRE_GetVersion

DESFire::StatusCode DESFire::MIFARE_DESFIRE_SelectApplication(mifare_desfire_tag* tag, mifare_desfire_aid_t* aid)
{
	StatusCode result;

	byte buffer[64];
	byte bufferSize = MIFARE_AID_SIZE;

	for (byte i = 0; i < MIFARE_AID_SIZE; i++) {
		buffer[i] = aid->data[i];
	}

	result = MIFARE_BlockExchangeWithData(tag, 0x5A, buffer, &bufferSize, buffer, &bufferSize);
	if (IsStatusCodeOK(result)) {
		// keep track of the application
		memcpy(tag->selected_application, aid->data, MIFARE_AID_SIZE);
	}

	return result;
}

DESFire::StatusCode DESFire::MIFARE_DESFIRE_GetFileIDs(mifare_desfire_tag* tag, byte* files, byte* filesCount)
{
	StatusCode result;

	byte bufferSize = MIFARE_MAX_FILE_COUNT + 5;
	byte buffer[bufferSize];

	result = MIFARE_BlockExchange(tag, 0x6F, buffer, &bufferSize);
	if (IsStatusCodeOK(result)) {
		*filesCount = bufferSize;
		memcpy(files, &buffer, *filesCount);
	}

	return result;
} // End MIFARE_DESFIRE_GetFileIDs

DESFire::StatusCode DESFire::MIFARE_DESFIRE_GetFileSettings(mifare_desfire_tag* tag, byte* file, mifare_desfire_file_settings_t* fileSettings)
{
	StatusCode result;

	byte buffer[21];
	byte bufferSize = 21;
	byte sendLen = 1;

	buffer[0] = *file;

	result = MIFARE_BlockExchangeWithData(tag, 0xF5, buffer, &sendLen, buffer, &bufferSize);
	if (IsStatusCodeOK(result)) {
		fileSettings->file_type = buffer[0];
		fileSettings->communication_settings = buffer[1];
		fileSettings->access_rights = ((uint16_t)(buffer[2]) << 8) | (buffer[3]);

		switch (buffer[0]) {
		case MDFT_STANDARD_DATA_FILE:
		case MDFT_BACKUP_DATA_FILE:
			fileSettings->settings.standard_file.file_size = ((uint32_t)(buffer[4])) | ((uint32_t)(buffer[5]) << 8) | ((uint32_t)(buffer[6]) << 16);
			break;

		case MDFT_VALUE_FILE_WITH_BACKUP:
			fileSettings->settings.value_file.lower_limit = ((uint32_t)(buffer[4])) | ((uint32_t)(buffer[5]) << 8) | ((uint32_t)(buffer[6]) << 16) | ((uint32_t)(buffer[7]) << 24);
			fileSettings->settings.value_file.upper_limit = ((uint32_t)(buffer[8])) | ((uint32_t)(buffer[9]) << 8) | ((uint32_t)(buffer[10]) << 16) | ((uint32_t)(buffer[11]) << 24);
			fileSettings->settings.value_file.limited_credit_value = ((uint32_t)(buffer[12])) | ((uint32_t)(buffer[13]) << 8) | ((uint32_t)(buffer[14]) << 16) | ((uint32_t)(buffer[15]) << 24);
			fileSettings->settings.value_file.limited_credit_enabled = buffer[16];
			break;

		case MDFT_LINEAR_RECORD_FILE_WITH_BACKUP:
		case MDFT_CYCLIC_RECORD_FILE_WITH_BACKUP:
			fileSettings->settings.record_file.record_size = ((uint32_t)(buffer[4])) | ((uint32_t)(buffer[5]) << 8) | ((uint32_t)(buffer[6]) << 16);
			fileSettings->settings.record_file.max_number_of_records = ((uint32_t)(buffer[7])) | ((uint32_t)(buffer[8]) << 8) | ((uint32_t)(buffer[9]) << 16);
			fileSettings->settings.record_file.current_number_of_records = ((uint32_t)(buffer[10])) | ((uint32_t)(buffer[11]) << 8) | ((uint32_t)(buffer[12]) << 16);
			break;

		default:
			//return FAIL;
			result.mfrc522 = STATUS_ERROR;
			return result;
		}
	}

	return result;
} // End MIFARE_DESFIRE_GetFileSettings

DESFire::StatusCode DESFire::MIFARE_DESFIRE_GetKeySettings(mifare_desfire_tag* tag, byte* settings, byte* maxKeys)
{
	StatusCode result;

	byte buffer[7];
	byte bufferSize = 7;

	result = MIFARE_BlockExchange(tag, 0x45, buffer, &bufferSize);
	if (IsStatusCodeOK(result)) {
		*settings = buffer[0];
		*maxKeys = buffer[1];
	}

	return result;
} // End MIFARE_DESFIRE_GetKeySettings()

DESFire::StatusCode DESFire::MIFARE_DESFIRE_GetKeyVersion(mifare_desfire_tag* tag, byte key, byte* version)
{
	StatusCode result;

	byte buffer[6];
	byte bufferSize = 6;
	byte sendLen = 1;

	buffer[0] = key;

	result = MIFARE_BlockExchangeWithData(tag, 0x64, buffer, &sendLen, buffer, &bufferSize);
	if (IsStatusCodeOK(result)) {
		*version = buffer[0];
	}

	return result;
}

DESFire::StatusCode DESFire::MIFARE_DESFIRE_ReadData(mifare_desfire_tag* tag, byte fid, uint32_t offset, uint32_t length, byte* backData, size_t* backLen)
{
	StatusCode result;

	byte buffer[64];
	byte bufferSize = 64;
	byte sendLen = 7;
	size_t outSize = 0;

	// file ID
	buffer[0] = fid;
	// offset
	buffer[1] = (offset & 0x00000F);
	buffer[2] = (offset & 0x00FF00) >> 8;
	buffer[3] = (offset & 0xFF0000) >> 16;
	// length
	buffer[4] = (length & 0x0000FF);
	buffer[5] = (length & 0x00FF00) >> 8;
	buffer[6] = (length & 0xFF0000) >> 16;

	result = MIFARE_BlockExchangeWithData(tag, 0xBD, buffer, &sendLen, buffer, &bufferSize);
	if (result.mfrc522 == STATUS_OK || result.desfire == MF_OPERATION_OK) {
		do {
			// Copy the data
			memcpy(backData + outSize, buffer, bufferSize);
			outSize += bufferSize;
			*backLen = outSize;

			if (result.desfire == MF_ADDITIONAL_FRAME) {
				result = MIFARE_BlockExchange(tag, 0xAF, buffer, &bufferSize);
			}
		} while (result.mfrc522 == STATUS_OK && result.desfire == MF_ADDITIONAL_FRAME);
	}

	return result;
}

DESFire::StatusCode DESFire::MIFARE_DESFIRE_GetValue(mifare_desfire_tag* tag, byte fid, int32_t* value)
{
	StatusCode result;

	byte buffer[MFRC522::FIFO_SIZE];
	byte bufferSize = MFRC522::FIFO_SIZE;
	byte sendLen = 1;
	size_t outSize = 0;

	buffer[0] = fid;

	result = MIFARE_BlockExchangeWithData(tag, 0x6C, buffer, &sendLen, buffer, &bufferSize);
	if (IsStatusCodeOK(result)) {
		*value = ((uint32_t)buffer[0] | ((uint32_t)buffer[1] << 8) | ((uint32_t)buffer[2] << 16) | ((uint32_t)buffer[3] << 24));
	}

	return result;
} // End MIFARE_DESFIRE_GetValue()

DESFire::StatusCode DESFire::MIFARE_DESFIRE_GetApplicationIds(mifare_desfire_tag* tag, mifare_desfire_aid_t* aids, byte* applicationCount)
{
	StatusCode result;

	// MIFARE_MAX_APPLICATION_COUNT * MIFARE_AID_SIZE + PCB (1 byte) + CID (1 byte) + Checksum (2 bytes)
	// I also add an extra byte in case NAD is needed
	byte bufferSize = (MIFARE_MAX_APPLICATION_COUNT * MIFARE_AID_SIZE) + 5;
	byte buffer[bufferSize];
	byte aidBuffer[MIFARE_MAX_APPLICATION_COUNT * MIFARE_AID_SIZE];
	byte aidBufferSize = 0;

	result = MIFARE_BlockExchange(tag, 0x6A, buffer, &bufferSize);
	if (result.mfrc522 != STATUS_OK)
		return result;

	// MIFARE_MAX_APPLICATION_COUNT (28) * MIFARE_AID_SIZE + PCB (1) + CID (1) + Checksum (2) = 88
	// Even if the NAD byte is not present we could GET a 0xAF response.
	if (result.desfire == MF_OPERATION_OK && bufferSize == 0x00) {
		// Empty application list
		*applicationCount = 0;
		return result;
	}

	memcpy(aidBuffer, buffer, bufferSize);
	aidBufferSize = bufferSize;

	while (result.desfire == MF_ADDITIONAL_FRAME) {
		bufferSize = (MIFARE_MAX_APPLICATION_COUNT * MIFARE_AID_SIZE) + 5;
		result = MIFARE_BlockExchange(tag, 0xAF, buffer, &bufferSize);
		if (result.mfrc522 != STATUS_OK)
			return result;

		// Make sure we have space (Just in case)
		if ((aidBufferSize + bufferSize) > (MIFARE_MAX_APPLICATION_COUNT * MIFARE_AID_SIZE)) {
			result.mfrc522 = STATUS_NO_ROOM;
			return result;
		}

		// Append the new data
		memcpy(aidBuffer + aidBufferSize, buffer, bufferSize);
	}


	// Applications are identified with a 3 byte application identifier(AID)
	// we also received the status byte:
	if ((aidBufferSize % 3) != 0) {
		printf("MIFARE_DESFIRE_GetApplicationIds(): Data is not a modulus of 3.");
		// TODO: Some kind of failure
		result.mfrc522 = STATUS_ERROR;
		return result;
	}

	*applicationCount = aidBufferSize / 3;

	for (byte i = 0; i < *applicationCount; i++) {
		aids[i].data[0] = aidBuffer[(i * 3)];
		aids[i].data[1] = aidBuffer[1 + (i * 3)];
		aids[i].data[2] = aidBuffer[2 + (i * 3)];
	}

	return result;
} // End MIFARE_DESFIRE_GetApplicationIds()



bool DESFire::IsStatusCodeOK(StatusCode code)
{
	if (code.mfrc522 != STATUS_OK)
		return false;
	if (code.desfire != MF_OPERATION_OK)
		return false;

	return true;
} // End IsStatusCodeOK();

void DESFire::PICC_DumpMifareDesfireMasterKey(mifare_desfire_tag* tag)
{

} // End PICC_DumpMifareDesfireMasterKey()

void DESFire::PICC_DumpMifareDesfireVersion(mifare_desfire_tag* tag, MIFARE_DESFIRE_Version_t* versionInfo)
{
	printf("-- Desfire Information --------------------------------------");
	printf("-------------------------------------------------------------");
	switch (versionInfo->hardware.version_major) {
	case 0x00:
		printf("  Card type          : MIFARE DESFire (MF3ICD40)");
		switch (versionInfo->hardware.storage_size) {
		case 0x16:
			printf(" 2K");
			break;
		case 0x18:
			printf(" 4K");
			break;
		case 0x1A:
			printf(" 8K");
			break;
		}
		printf("\n");
		break;
	case 0x01:
		printf("  Card type          : MIFARE DESFire EV1");
		switch (versionInfo->hardware.storage_size) {
		case 0x16:
			printf(" 2K");
			break;
		case 0x18:
			printf(" 4K");
			break;
		case 0x1A:
			printf(" 8K");
			break;
		}
		printf("\n");
		break;
	case 0x12:
		printf("  Card type          : MIFARE DESFire EV2");
		switch (versionInfo->hardware.storage_size) {
		case 0x16:
			printf(" 2K");
			break;
		case 0x18:
			printf(" 4K");
			break;
		case 0x1A:
			printf(" 8K");
			break;
		}
		printf("\n");
		break;
	}

	// UID
	printf("  UID                :");
	for (byte i = 0; i < 7; i++) {
		if (versionInfo->uid[i] < 0x10)
			printf(" 0");
		else
			printf(" ");
		printf("%X", versionInfo->uid[i]);
	}
	printf("\n");

	// Batch
	printf("  Batch number       :");
	for (byte i = 0; i < 5; i++) {
		if (versionInfo->batch_number[i] < 0x10)
			printf(" 0");
		else
			printf(" ");
		printf("%X", versionInfo->batch_number[i]);
	}
	printf("\n");

	printf("  Production week    : 0x");
	if (versionInfo->production_week < 0x10)
		printf("0");
	printf("%X", versionInfo->production_week);

	printf("  Production year    : 0x");
	if (versionInfo->production_year < 0x10)
		printf("0");
	printf("%X", versionInfo->production_year);

	printf("  ----------------------------------------------------------");
	printf("  Hardware Information");
	printf("      Vendor ID      : 0x");
	if (versionInfo->hardware.vendor_id < 0x10)
		printf("0");
	printf("%X", versionInfo->hardware.vendor_id);
	if (versionInfo->hardware.vendor_id == 0x04)
		printf(" (NXP)");
	printf("\n");

	printf("      Type           : 0x");
	if (versionInfo->hardware.type < 0x10)
		printf("0");
	printf("%X", versionInfo->hardware.type);

	printf("      Subtype        : 0x");
	if (versionInfo->hardware.subtype < 0x10)
		printf("0");
	printf("%X", versionInfo->hardware.subtype);

	printf("      Version        : ");
	printf("%X", versionInfo->hardware.version_major);
	printf(".");
	printf("%X", versionInfo->hardware.version_minor);

	printf("      Storage size   : 0x");
	if (versionInfo->hardware.storage_size < 0x10)
		printf("0");
	printf("%X", versionInfo->hardware.storage_size);
	switch (versionInfo->hardware.storage_size) {
	case 0x16:
		printf(" (2048 bytes)");
		break;
	case 0x18:
		printf(" (4096 bytes)");
		break;
	case 0x1A:
		printf(" (8192 bytes)");
		break;
	}
	printf("\n");

	printf("      Protocol       : 0x");
	if (versionInfo->hardware.protocol < 0x10)
		printf("0");
	printf("%X", versionInfo->hardware.protocol);

	printf("  ----------------------------------------------------------");
	printf("  Software Information");
	printf("      Vendor ID      : 0x");
	if (versionInfo->software.vendor_id < 0x10)
		printf("0");
	printf("%X", versionInfo->software.vendor_id);
	if (versionInfo->software.vendor_id == 0x04)
		printf(" (NXP)");
	printf("\n");

	printf("      Type           : 0x");
	if (versionInfo->software.type < 0x10)
		printf("0");
	printf("%X", versionInfo->software.type);

	printf("      Subtype        : 0x");
	if (versionInfo->software.subtype < 0x10)
		printf("0");
	printf("%X", versionInfo->software.subtype);

	printf("      Version        : ");
	printf("%X", versionInfo->software.version_major);
	printf(".");
	printf("%X", versionInfo->software.version_minor);

	printf("      Storage size   : 0x");
	if (versionInfo->software.storage_size < 0x10)
		printf("0");
	printf("%X", versionInfo->software.storage_size);
	switch (versionInfo->software.storage_size) {
	case 0x16:
		printf(" (2048 bytes)");
		break;
	case 0x18:
		printf(" (4096 bytes)");
		break;
	case 0x1A:
		printf(" (8192 bytes)");
		break;
	}
	printf("\n");

	printf("      Protocol       : 0x");
	if (versionInfo->software.protocol < 0x10)
		printf("0");
	printf("%X", versionInfo->software.protocol);

	printf("-------------------------------------------------------------");
}

void DESFire::PICC_DumpMifareDesfireApplication(mifare_desfire_tag* tag, mifare_desfire_aid_t* aid)
{
	StatusCode response;

	printf("-- Desfire Application --------------------------------------");
	printf("-------------------------------------------------------------");
	printf("  AID                :");
	for (byte i = 0; i < 3; i++) {
		if (aid->data[i] < 0x10)
			printf(" 0");
		else
			printf(" ");
		printf("%X", aid->data[i]);
	}
	printf("\n");

	// Select the current application.
	response = MIFARE_DESFIRE_SelectApplication(tag, aid);
	if (!IsStatusCodeOK(response)) {
		printf("Error: Failed to select application.");
		//		printf(GetStatusCodeName(response));
		printf("-------------------------------------------------------------");
		return;
	}

	// Get Key settings
	byte keySettings;
	byte keyCount = 0;
	byte keyVersion[16];

	response = MIFARE_DESFIRE_GetKeySettings(tag, &keySettings, &keyCount);
	if (IsStatusCodeOK(response)) {
		printf("  Key settings       : 0x");
		if (keySettings < 0x10)
			printf("0");
		printf("%X", keySettings);

		printf("  Max num keys       : ");
		printf("%X", keyCount);

		// Get key versions (No output will be outputed later)
		for (byte ixKey = 0; ixKey < keyCount; ixKey++) {
			response = MIFARE_DESFIRE_GetKeyVersion(tag, ixKey, &(keyVersion[ixKey]));
			if (!IsStatusCodeOK(response))
				keyVersion[ixKey] = 0x00;
		}

	}
	else {
		printf("  Error: Failed to get application key settings.");
		// Just to be sure..
		keyCount = 0;
	}

	// Get the files
	byte files[MIFARE_MAX_FILE_COUNT];
	byte filesCount = 0;
	response = MIFARE_DESFIRE_GetFileIDs(tag, files, &filesCount);
	if (!IsStatusCodeOK(response)) {
		printf("  Error: Failed to get application file IDs.");
		printf("  ");
		//		printf(GetStatusCodeName(response));
		printf("-------------------------------------------------------------");
		return;
	}

	// Number of files
	printf("  Num. Files         : ");
	printf("%X", filesCount);

	// Output key versions
	if (keyCount > 0) {
		printf("  ----------------------------------------------------------");
		printf("  Key Versions");
		for (byte ixKey = 0; ixKey < keyCount; ixKey++) {
			printf("      Key 0x");
			if (ixKey < 0x10)
				printf("0");
			printf("%X", ixKey);
			printf("       : 0x");
			if (keyVersion[ixKey] < 0x10)
				printf("0");
			printf("%X", keyVersion[ixKey]);
		}
	}

	for (byte i = 0; i < filesCount; i++) {
		printf("  ----------------------------------------------------------");
		printf("  File Information");
		printf("      File ID        : 0x");
		if (files[i] < 0x10)
			printf("0");
		printf("%X", files[i]);

		// Get file settings
		mifare_desfire_file_settings_t fileSettings;

		response = MIFARE_DESFIRE_GetFileSettings(tag, &(files[i]), &fileSettings);
		if (IsStatusCodeOK(response)) {
			printf("      File Type      : 0x");
			if (fileSettings.file_type < 0x10)
				printf("0");
			printf("%X", fileSettings.file_type);
			printf(" (");
			//			printf("%X",GetFileTypeName((mifare_desfire_file_types)fileSettings.file_type));
			printf(")");

			printf("      Communication  : 0x");
			if (fileSettings.communication_settings < 0x10)
				printf("0");
			printf("%X", fileSettings.communication_settings);
			printf(" (");
			//			printf(GetCommunicationModeName((mifare_desfire_communication_modes)fileSettings.communication_settings));
			printf(")");

			printf("      Access rights  : 0x");
			printf("%X", fileSettings.access_rights);

			switch (fileSettings.file_type) {
			case MDFT_STANDARD_DATA_FILE:
			case MDFT_BACKUP_DATA_FILE:
				printf("      File Size      : ");
				printf("%X", fileSettings.settings.standard_file.file_size);
				printf(" bytes");
				break;
			case MDFT_VALUE_FILE_WITH_BACKUP:
				printf("      Lower Limit    : ");
				printf("%X", fileSettings.settings.value_file.lower_limit);
				printf("      Upper Limit    : ");
				printf("%X", fileSettings.settings.value_file.upper_limit);
				printf("      Limited credit : ");
				printf("%X", fileSettings.settings.value_file.limited_credit_value);
				printf("      Limited credit : ");

				if (fileSettings.settings.value_file.limited_credit_enabled == 0x00)
					printf("Disabled (");
				else
					printf("Enabled (0x");
				if (fileSettings.settings.value_file.limited_credit_enabled < 0x10)
					printf("0");
				printf("%X", fileSettings.settings.value_file.limited_credit_enabled);
				printf(")");

				break;

			case MDFT_LINEAR_RECORD_FILE_WITH_BACKUP:
			case MDFT_CYCLIC_RECORD_FILE_WITH_BACKUP:
				printf("      Record size    : ");
				printf("%X", fileSettings.settings.record_file.record_size);
				printf("      max num records: ");
				printf("%X", fileSettings.settings.record_file.max_number_of_records);
				printf("      num records    : ");
				printf("%X", fileSettings.settings.record_file.current_number_of_records);
				break;
			}

			switch (fileSettings.file_type) {
			case MDFT_STANDARD_DATA_FILE:
			case MDFT_BACKUP_DATA_FILE:
			{
				// Get file data
				byte fileContent[fileSettings.settings.standard_file.file_size];
				size_t fileContentLength = fileSettings.settings.standard_file.file_size;
				response = MIFARE_DESFIRE_ReadData(tag, files[i], 0, fileSettings.settings.standard_file.file_size, fileContent, &fileContentLength);
				if (response.mfrc522 == STATUS_OK) {
					printf("      ------------------------------------------------------");
					printf("      Data");

					if (response.desfire == MF_OPERATION_OK || response.desfire == MF_ADDITIONAL_FRAME) {
						for (unsigned int iByte = 0; iByte < fileContentLength; iByte++) {
							if ((iByte % 16) == 0) {
								if (iByte != 0)
									printf("\n");
								printf("           ");
							}
							if (fileContent[iByte] < 0x10)
								printf(" 0");
							else
								printf(" ");
							printf("%X", fileContent[iByte]);
						}
						printf("\n");
					}
					else {
						printf("           ");
						//							printf(GetStatusCodeName(response));
					}
				}
			}
			break;
			case MDFT_VALUE_FILE_WITH_BACKUP:
			{
				// Get value
				int32_t fileValue;
				response = MIFARE_DESFIRE_GetValue(tag, files[i], &fileValue);
				printf("      Value          : ");
				if (IsStatusCodeOK(response)) {
					printf("%X", fileValue);
				}
				else {
					//						printf(GetStatusCodeName(response));
				}
			}
			break;
			}

		}
		else {
			printf("      Error: Failed to get file settings.");
			printf("      ");
			//			printf(GetStatusCodeName(response));
		}
	}

	printf("-------------------------------------------------------------");
}