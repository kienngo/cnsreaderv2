
/*
 * Authors: Zeder
 *
 */

#include "string.h"
#include "spi.h"



void spi_transfer(uint8_t channel, unsigned char* txbuff, unsigned char* rxbuff, int len)
{
	int ret;
	struct spi_ioc_transfer spi_tr = {
		.tx_buf = (unsigned long)  txbuff,
		.rx_buf = (unsigned long)  rxbuff,
		.len = len,
		.delay_usecs = 0,
		.speed_hz = 900 * 1000,
		   // speed in KHz to Hz
		.bits_per_word = 8,
	};
	ret = ioctl(channel, SPI_IOC_MESSAGE(1), &spi_tr);
	if (ret < 1)
	{
		perror("Can't send spi message\n");
		close(channel);
		exit(2);
	}
}
uint8_t spi_transfera(uint8_t channel, unsigned char value)
{
	uint8_t ret;
	struct spi_ioc_transfer spi_tr = {
		.tx_buf = (unsigned long)  &value,
		.rx_buf = (unsigned long)  &value,
		.len = 1,
		.delay_usecs = 0,
		.speed_hz = 900 * 1000,
		// speed in KHz to Hz
	 .bits_per_word = 8,
	};
	ret = ioctl(channel, SPI_IOC_MESSAGE(1), &spi_tr);
	if (ret < 1)
	{
		perror("Can't send spi message\n");
		close(channel);
		exit(2);
	}
	else {
		//printf("send spi ok\n");
	}
	ret = value;
	return ret;
}

