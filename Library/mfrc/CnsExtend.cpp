

#include <iostream>
#include <string>


#include "CnsExtend.h"
#include "ultils.h"



//Public Function
//************************************
// Method:    CnsExtended
// FullName:  CnsExtended::CnsExtended
// Access:    public 
// Returns:   
// Qualifier:
// Parameter: DESFire mfrc
//************************************

CnsExtended::~CnsExtended(){
	this->_mfrc_cnsextend.PICC_HaltA();
	this->_mfrc_cnsextend.PCD_StopCrypto1();
}
std::string CnsExtended::CnsGetName()
{
	switch (this->_mfrc_cnsextend.PICC_GetType(this->_mfrc_cnsextend.uid.sak))
	{
		case MFRC522::PICC_TYPE_MIFARE_1K:
			return Classic_ReadName();
			break;
		case MFRC522::PICC_TYPE_ISO_14443_4:
			return Mifare_ReadName();
			break;
	}
	return std::string();
}

std::string CnsExtended::CnsGetCardNumber()
{
	switch (this->_mfrc_cnsextend.PICC_GetType(this->_mfrc_cnsextend.uid.sak))
	{
		case MFRC522::PICC_TYPE_MIFARE_1K:
			return Classic_ReadCardNumber();
			break;
		case MFRC522::PICC_TYPE_ISO_14443_4:
			return Mifare_ReadCardNumber();
			break;
		default:
			return std::string();
	}
}

std::string CnsExtended::CnsGetPocket()
{
	return std::string();
}


bool CnsExtended::StartReadCard()
{
	if (!this->_mfrc_cnsextend.FastDetect())
		return false;
	if (!this->_mfrc_cnsextend.PICC_ReadCardSerial())
		return false;
	return true;
}

//Private Fucntion
std::string CnsExtended::Classic_ReadName()
{
	uint8_t buffer[18] = { 0 };
	uint8_t byteCount = 18;

	uint8_t blockAddr = 0x08;
	DESFire::MIFARE_Key key;
	// Read block
	std::string name_str;
	//byte[] d = { (byte)0xC1, (byte)0x2A, (byte)0x7B, (byte)0xEE, (byte)0xFF, (byte)0x3D };
	byte password[6] = { (byte)0xC1, (byte)0x2A, (byte)0x7B, (byte)0xEE, (byte)0xFF, (byte)0x3D };

	Classic_GetDiversify(this->_mfrc_cnsextend.uid, &key, password);
	byte name_content[60];
	uint8_t name_index = 0;

	byte status = this->_mfrc_cnsextend.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockAddr, &key, &(this->_mfrc_cnsextend.uid));
	if (status != MFRC522::STATUS_OK) {
		printf("PCD_Authenticate() failed NEW(read): ");
		printf("%s", this->_mfrc_cnsextend.GetStatusCodeName((MFRC522::StatusCode)status).c_str());
		return "";
	}

	byteCount = 18;
	status = this->_mfrc_cnsextend.MIFARE_Read(9, buffer, &byteCount);
	if (status != MFRC522::STATUS_OK) {
		printf("MIFARE_Read() failed: ");
		printf("%s\n", this->_mfrc_cnsextend.GetStatusCodeName(status).c_str());
		return "";
	}


	name_str.append(std::string((char*)buffer), 0, 16);
	std::cout << name_str << '\n';

	byteCount = 18;
	status = this->_mfrc_cnsextend.MIFARE_Read(10, buffer, &byteCount);
	if (status != MFRC522::STATUS_OK) {
		printf("MIFARE_Read() failed: ");
		printf("%s\n", this->_mfrc_cnsextend.GetStatusCodeName(status).c_str());

		return "";
	}

	name_str.append(std::string((char*)buffer), 0, 16);
	blockAddr = 0x0C;
	status = this->_mfrc_cnsextend.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockAddr, &key, &(this->_mfrc_cnsextend.uid));
	if (status != MFRC522::STATUS_OK) {
		printf("PCD_Authenticate() failed NEW(read): ");
		printf("%s", this->_mfrc_cnsextend.GetStatusCodeName((MFRC522::StatusCode)status).c_str());

		return "";
	}

	byteCount = 18;
	status = this->_mfrc_cnsextend.MIFARE_Read(12, buffer, &byteCount);

	if (status != MFRC522::STATUS_OK) {
		printf("MIFARE_Read() failed: ");
		printf("%s\n", this->_mfrc_cnsextend.GetStatusCodeName(status).c_str());
		//mfrc.PCD_StopCrypto1();
		return "";
	}

	name_str.append(std::string((char*)buffer), 0, 16);

	byteCount = 18;
	status = this->_mfrc_cnsextend.MIFARE_Read(13, buffer, &byteCount);
	if (status != MFRC522::STATUS_OK) {
		printf("MIFARE_Read() failed: ");
		printf("%s\n", this->_mfrc_cnsextend.GetStatusCodeName(status).c_str());
		//mfrc.PCD_StopCrypto1();
		return "";
	}


	name_str.append(std::string((char*)buffer), 0, 16);

	std::cout << name_str << '\n';
	std::cout << "name size" << name_str.size() << '\n';

	int index_of_endname = name_str.find_first_of('*');
	name_str = name_str.substr(0, index_of_endname);

	std::cout << name_str << '\n';
	std::cout << "name size after substr" << name_str.length() << '\n';

	//name_str = name_str.substr(0, name_str.size() - 1);


	std::wstring str_turned_to_wstr = std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(name_str.c_str());
	//std::wstring str_turned_to_wstr = std::wstring_convert<std::codecvt_utf8<wchar_t>>().;
	str_turned_to_wstr = RemoveVietnameseTone(str_turned_to_wstr);

	name_str = std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(str_turned_to_wstr);


	std::cout << "namestr final: " << name_str << '\n';
	int whitespace_cout = 0;
	for (int i = 0; i < strlen(name_str.c_str()); i++)
	{
		if (isspace(name_str[i])) whitespace_cout++;

	}
	std::cout << "whitespace: " << whitespace_cout << '\n';

	if (whitespace_cout <= 1) {
		return name_str;
	}
	uint8_t m_index = name_str.find_last_of(" ");
	std::string middle_name = name_str.substr(0, m_index);

	middle_name = middle_name.substr(middle_name.find_last_of(" "), middle_name.size() - middle_name.find_last_of(" "));
	name_str = name_str.substr(m_index, name_str.size() - m_index);

	name_str = middle_name.append(name_str);
	std::cout << name_str << '\n';

	return name_str;
}

std::string CnsExtended::Classic_ReadData()
{
	return std::string();
}

std::string CnsExtended::Classic_ReadCardNumber()
{
	uint8_t blockAddr  = 4;
	DESFire::MIFARE_Key key;
	byte password[6] = { (byte)0xEE, (byte)0xFF, (byte)0x3D, (byte)0xC1, (byte)0x2A, (byte)0x7B };
	Classic_GetDiversify(this->_mfrc_cnsextend.uid, &key, password);

	byte  status = this->_mfrc_cnsextend.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockAddr, &key, &(this->_mfrc_cnsextend.uid));
	if (status != MFRC522::STATUS_OK) {
		printf("PCD_Authenticate() failed NEW(read): ");
		printf("%s", this->_mfrc_cnsextend.GetStatusCodeName((MFRC522::StatusCode)status).c_str());
		return "";
	}
	// Read block
	std::string name_str;
	uint8_t buffer[18] = { 0};
	uint8_t byteCount = sizeof(buffer);
	
	status = this->_mfrc_cnsextend.MIFARE_Read(4, buffer, &byteCount);
	if (status != MFRC522::STATUS_OK) {
		printf("MIFARE_Read() failed: ");
		printf("%s\n", this->_mfrc_cnsextend.GetStatusCodeName(status).c_str());
		return "";
	}
	

	std::string cardNumber;
	char tmp_card[20];
	for (uint8_t i = 0; i < byteCount; i++)
	{
		sprintf(tmp_card, "%02X", buffer[i]);
		cardNumber.append(tmp_card);
	}
	printf("\n");
	cardNumber = cardNumber.substr(9, 19);
	std::cout << cardNumber << '\n';
	return cardNumber;
}

std::string CnsExtended::Classic_ReadValue()
{
	return std::string();
}

std::string CnsExtended::Mifare_ReadName()
{
	DESFire::mifare_desfire_tag tag;
	DESFire::StatusCode response;
	DESFire::mifare_desfire_aid_t aids[MIFARE_MAX_APPLICATION_COUNT];

	tag.pcb = 0x0A;
	tag.cid = 0x00;
	memset(tag.selected_application, 0, 3);
	// Make sure non DESFire status codes have DESFireStatus code to OK
	response.desfire = DESFire::MF_OPERATION_OK;


	aids[0].data[0] = 0x03;
	aids[0].data[1] = 0x00;
	aids[0].data[2] = 0xDF;

	response = this->_mfrc_cnsextend.MIFARE_DESFIRE_SelectApplication(&tag, aids);
	if (!this->_mfrc_cnsextend.IsStatusCodeOK(response)) {
		printf("Failed to select application IDs! \n");
		return std::string();
	}
	byte name_content[80];
	size_t fileNameLength = 60;

	response = this->_mfrc_cnsextend.MIFARE_DESFIRE_ReadData(&tag, 0x01, 6, fileNameLength, name_content, &fileNameLength);


	if (!this->_mfrc_cnsextend.IsStatusCodeOK(response)) {
		printf("Failed to read name! \n");
		return std::string();
	}

	std::string name_str = std::string((char*)name_content);
	std::cout << "name size" << name_str.size() << '\n';
	name_str = name_str.substr(0, name_str.size());

	std::wstring str_turned_to_wstr = std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(name_str);
	str_turned_to_wstr = RemoveVietnameseTone(str_turned_to_wstr);

	name_str = std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(str_turned_to_wstr);

	int whitespace_cout = 0;
	for (int i = 0; i < strlen(name_str.c_str()); i++)
	{
		if (isspace(name_str[i])) whitespace_cout++;

	}
	std::cout << "whitespace: " << whitespace_cout << '\n';

	if (whitespace_cout <= 1) {
		return name_str;
	}
	uint8_t m_index = name_str.find_last_of(" ");
	std::string middle_name = name_str.substr(0, m_index);
	middle_name = middle_name.substr(middle_name.find_last_of(" "), middle_name.size() - middle_name.find_last_of(" "));
	name_str = name_str.substr(m_index, name_str.size() - m_index - 1);

	name_str = middle_name.append(name_str);

	std::cout << name_str << '\n';
	return name_str;
}

std::string CnsExtended::Mifare_ReadCardNumber()
{
	// Desfire communication
	DESFire::mifare_desfire_tag tag {};
	DESFire::StatusCode response {}	;

	// Make sure non DESFire status codes have DESFireStatus code to OK
	response.desfire = DESFire::MF_OPERATION_OK;

	byte ats[16];
	byte atsLength = 16;
	
	response.mfrc522 = this->_mfrc_cnsextend.PICC_RequestATS(ats, &atsLength);
	if (!this->_mfrc_cnsextend.IsStatusCodeOK(response)) {
		printf("Read card failed\n");
		return std::string();
	}
	tag.pcb = 0x0A;
	tag.cid = 0x00;
	memset(tag.selected_application, 0, 3);

	DESFire::mifare_desfire_aid_t aids[MIFARE_MAX_APPLICATION_COUNT];
			
	aids[0].data[0] = 0x01;
	aids[0].data[1] = 0x00;
	aids[0].data[2] = 0xDF;

	response = this->_mfrc_cnsextend.MIFARE_DESFIRE_SelectApplication(&tag, aids);
	if (!this->_mfrc_cnsextend.IsStatusCodeOK(response)) {
		printf("Failed to select application IDs! \n");
		return std::string();
	}
	byte fileContent[40];
	size_t fileContentLength = 10;

	response = this->_mfrc_cnsextend.MIFARE_DESFIRE_ReadData(&tag, 0x01, 4, 10, fileContent, &fileContentLength);

	
	if (!this->_mfrc_cnsextend.IsStatusCodeOK(response)) {
		printf("Failed to read data! \n");
		return std::string();
	}
	
	

	std::string cardNumber;
	char tmp_card[40];
	for (uint8_t i = 0; i < 10; i++)
	{
		sprintf(tmp_card, "%02X", fileContent[i]);
		cardNumber.append(tmp_card);
		
	}
	// Store Card in log attendance
	cardNumber = cardNumber.substr(1, cardNumber.length());
	
	std::cout << cardNumber << '\n';
	
	return cardNumber;
}

std::string CnsExtended::Mifare_ReadData()
{
	return std::string();
}

std::string CnsExtended::Mifare_ReadValue()
{
	return std::string();
}


//For MFRC Ultils
void  Classic_GetDiversify(MFRC522::Uid uid, DESFire::MIFARE_Key* key, uint8_t password[])
{

	byte midle[6], final[6];
	memcpy(midle, uid.uidByte, uid.size);
	midle[4] = midle[2];
	midle[5] = midle[3];
	for (int i = 0; i < 6; i++)
	{
		final[i] = (byte)(password[i] ^ midle[i]);
	}
	memcpy(key->keyByte, final, 6);
}
