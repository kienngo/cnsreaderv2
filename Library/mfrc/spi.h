#ifndef SPI_H_
#define SPI_H_
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "string.h"
#include "wiringPi.h"
#include "wiringPiSPI.h"

#ifdef __cplusplus
extern "C" {
#endif
	void spi_transfer(uint8_t channel, unsigned char *txbuff, unsigned char *rxbuff, int len);
	uint8_t spi_transfera(uint8_t channel, unsigned char value);
	
	
#ifdef __cplusplus
}
#endif
#endif /* SPI_H_ */