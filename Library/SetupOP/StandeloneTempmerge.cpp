#include <iostream>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <cstring>
#include <errno.h>
#include <curl/curl.h>
#include <httpHelper.h>
#include "wiringPi.h"
#include "wiringPiSPI.h"
#include "wiringPiI2C.h"
#include "mfrc522cpp.h"
#include "Desfire.h"
#include "pthread.h"
#include "Desfire.h"
#include "softTone.h"
//#include "time.h"
#include "gpio_external.h"
#include <bits/stdc++.h> 
#include "Support/support.h"
#include <signal.h>
#include <sys/time.h>
#include "pcf8574.h"
#include "lcd/lcd.h"
#include "configMachine.hpp"
#include <wiringSerial.h>
#include "api/api.h"
//#include <chrono.h>;
using namespace Json;

#define TIME_INTERVAL_KEEPALIVE 55*1000
#define TIME_INTERVAL_POSTATTENDANCE 10*1000
#define TIME_INTERVAL  300*1000    //( 1000 = 1s)
#define TIME_BUZZER    500

#define BUZZER  7
#define RELAY   6

#define LED_R   5
#define LED_G   4

static const char       *usbDev0  = "/dev/ttyUSB0";
static const char       *OP  = "OP\r";


extern uint8_t Rc522_fd;
uint8_t buzzer_en = 0;
uint8_t counter = 0;
uint8_t ledred_en = 0;
uint8_t ledgreen_en = 0;
//log attendance is busy ?
uint8_t log_attendance_bs = 0;
uint16_t coutdown = 0;
uint8_t flag_buzzer = 0;
int usbFd;
//DESFire mfrc;
//W25qxx  w25qxx;
pthread_t thread_main, thread_timer,
		  thread_whitelist, 
		  thread_buzzer ,
		  thread_keep_alive, 
		  thread_post_attendance,
	      thread_led;

int thread_buzzer_id;
int thread_led_id;
int count = 0;
struct itimerval intervaltime;
//void* printf_main_message(void* arg);
void relay_init();
void led_init();
void* main_function(void* arg);
void* timer_function(void* arg);
void* whitelist_function(void* arg);
void* buzzer_function(void* arg);
void* led_function(void* arg);

void* post_attendace_function(void* arg);
void* keep_alive(void* arg);
void* keep_alive_v2(void* arg);
byte check_in_whitelist(char* card_number);


void remove_line(std::string filepath, int num_line_remove);
void delete_line(const char *file_name, int n);
void copy_contents_file(const char *source, const char *dest);
std::wstring RemoveVietnameseTone(const std::wstring& text);
void timer_handler(int sig);
void GPIO_OpenGate();

// Global
DESFire mfrc;

buzzer _buzzer;
relay  _relay;
configMachine config;

void initial_hardware();
void read_card();
std::string  MifareService();
std::string ClassicService();
void Classic_GetDiversify(MFRC522::Uid uid, DESFire::MIFARE_Key *key);

//MIFARE AND CLASSIC
std::string MifareService()
{
	// Desfire communication
	DESFire::mifare_desfire_tag tag;
	DESFire::StatusCode response;

	tag.pcb = 0x0A;
	tag.cid = 0x00;
	memset(tag.selected_application, 0, 3);

	// Make sure non DESFire status codes have DESFireStatus code to OK
	response.desfire = DESFire::MF_OPERATION_OK;

	byte ats[16];
	byte atsLength = 16;
	
	response.mfrc522 = mfrc.PICC_RequestATS(ats, &atsLength);
	if (!mfrc.IsStatusCodeOK(response)) {

		printf("Read card failed\n");
		// mfrc522.PICC_HaltA();
		return std::string();
	}

	DESFire::mifare_desfire_aid_t aids[MIFARE_MAX_APPLICATION_COUNT];
	//	byte applicationCount = 0;
		aids[0].data[0] = 0x01;
	aids[0].data[1] = 0x00;
	aids[0].data[2] = 0xDF;

	response = mfrc.MIFARE_DESFIRE_SelectApplication(&tag, aids);
	if (!mfrc.IsStatusCodeOK(response)) {
		printf("Failed to select application IDs! \n");
		return std::string();
	}
	byte fileContent[40];
	size_t fileContentLength = 10;

	response = mfrc.MIFARE_DESFIRE_ReadData(&tag, 0x01, 4, 10, fileContent, &fileContentLength);

	
	if (!mfrc.IsStatusCodeOK(response)) {
		printf("Failed to read data! \n");
		return std::string();
	}
	
	

	std::string cardNumber;
	char tmp_card[40];
	for (uint8_t i = 0; i < 10; i++)
	{
		sprintf(tmp_card, "%02X", fileContent[i]);
		cardNumber.append(tmp_card);
		
	}
	// Store Card in log attendance
	cardNumber = cardNumber.substr(1, cardNumber.length());
	
	std::cout << cardNumber << std::endl;
	
	return cardNumber;
}

void  Classic_GetDiversify(MFRC522::Uid uid, DESFire::MIFARE_Key *key)
{
	byte a[6] = { (byte) 0xEE, (byte)0xFF, (byte)0x3D, (byte)0xC1, (byte)0x2A, (byte)0x7B };
	byte b[6], c[6];
	memcpy(b, uid.uidByte, uid.size);
	b[4] = b[2];
	b[5] = b[3];
//	printf("Key \n");
	for (int i = 0; i < 6; i++)
	{
		c[i] = (byte)(a[i] ^ b[i]);
//		printf("%x ", c[i]);
	}
//	printf("\n");
	memcpy(key->keyByte, c, 6);
}

std::string ClassicService()
{
	uint8_t blockAddr  = 4;
	DESFire::MIFARE_Key key;
	Classic_GetDiversify(mfrc.uid, &key);
//	mfrc.PICC_DumpMifareClassicSectorToSerial(&(mfrc.uid), &key, 1);
//	mfrc.PCD_StopCrypto1();
	
//	return;
	byte  status = mfrc.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockAddr, &key, &(mfrc.uid));
	if (status != MFRC522::STATUS_OK) {
		printf("PCD_Authenticate() failed NEW(read): ");
		printf("%s", mfrc.GetStatusCodeName((MFRC522::StatusCode)status).c_str());
		return "";
	}
	// Read block
	uint8_t buffer[18];
	uint8_t byteCount = sizeof(buffer);
	status = mfrc.MIFARE_Read(4, buffer, &byteCount);
	if (status != MFRC522::STATUS_OK) {
		printf("MIFARE_Read() failed: ");
		printf("%s\n",mfrc.GetStatusCodeName(status).c_str());
		mfrc.PCD_StopCrypto1();
		return "";
	}
	
	mfrc.PICC_HaltA();
	mfrc.PCD_StopCrypto1();
	

	std::string cardNumber;
	char tmp_card[20];
	for (uint8_t i = 0; i < byteCount; i++)
	{
		sprintf(tmp_card, "%02X", buffer[i]);
		cardNumber.append(tmp_card);
	}
	printf("\n");
	cardNumber = cardNumber.substr(9,19);
	std::cout << cardNumber << std::endl;
	return cardNumber;
	
}
int main() {
	std::signal(SIGUSR1, signal_handler);
	
	char* p;
	char sn_str[23];	
	uint16_t CType = 0;
	char status;
	int tmp, i;
	struct itimerval timer;
	
	int bits, rows, cols;
	printf("Init \n");
//	config = configMachine("PRODUCT");
	config = configMachine("UAT");

	initial_hardware();
	mfrc = DESFire(&Rc522_fd);
	mfrc.PCD_Init();
 

	buzzer_en = 1;		
	
//	int thread_main_id = pthread_create(&thread_main, nullptr, main_function, nullptr);
	thread_buzzer_id = pthread_create(&thread_buzzer, nullptr, buzzer_function, nullptr);
	
//	int thread_whitelist_id = pthread_create(&thread_whitelist, nullptr, whitelist_function, nullptr);
//	int thread_keep_alive_id = pthread_create(&thread_keep_alive, nullptr, keep_alive_v2, nullptr);	
	int thread_keep_alive_id = pthread_create(&thread_keep_alive, nullptr, keep_alive, nullptr);
	int thread_post_attendace_id = pthread_create(&thread_post_attendance, nullptr, post_attendace_function, nullptr);
	//	int thread_led_id  = pthread_create(&thread_led, nullptr, led_function, nullptr);
	

	
	
	for (;;) {
		read_card();
		delay(50);
	}
}

/**********************************************************************************************//**
 * \fn	void relay_init(void)
 *
 * \brief	Relay initialize
 *
 * \author	Zeder
 * \date	6/29/2020
 **************************************************************************************************/

void relay_init()
{
	pinMode(RELAY, OUTPUT);
	digitalWrite(RELAY, LOW);
	
}
/**********************************************************************************************//**
 * \fn	void relay_init(void)
 *
 * \brief	LED initialize
 *
 * \author	Zeder
 * \date	8/29/2020
 ************************************************************************************************/

void led_init()
{
	pinMode(LED_R, OUTPUT);
	pinMode(LED_G, OUTPUT);
	digitalWrite(LED_R, HIGH);
	digitalWrite(LED_G, HIGH);
}


/**********************************************************************************************//**
 * \fn	void* main_function(void* arg)
 *
 * \brief	Main function
 *
 * \author	Zeder
 * \date	6/23/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **************************************************************************************************/

void* main_function(void* arg)
{
	FILE* fptr;
	byte dumpSerial;
	time_t t = time(nullptr);
	printf("main fucntion \n");
	printf("detect card\n");
	for (;;) {
		dumpSerial = mfrc.PCD_DumpVersionToSerial();
		if ((0xFF == dumpSerial) || (0x00 == dumpSerial)) {
			
//			struct tm* tm = localtime(&t);
//			fptr = fopen("logRS.txt", "a+");
//			fprintf(fptr,"reset at : %s \n", asctime(tm));
//			fclose(fptr);
//
//			log_reader("mfrc", "reset antenna");
			mfrc.PCD_AntennaOff();
			mfrc.PCD_Reset();
			//mfrc.PCD_Init();
		}
		read_card();
	}
}

	if (1 == ledred_en)
	{
			digitalWrite(LED_R, LOW);
			digitalWrite(LED_G, HIGH);
			delay(3500);
			digitalWrite(LED_G, LOW);
			delay(100);
			digitalWrite(LED_R, HIGH);
			ledred_en = 0;
	}
	else if (2 == ledgreen_en)
	{
			digitalWrite(LED_G, LOW);
			digitalWrite(LED_R, LOW);
			delay(300);
			digitalWrite(LED_R, HIGH);
			delay(300);
			digitalWrite(LED_R, LOW);
			delay(300);
			digitalWrite(LED_R, HIGH);
			delay(300);
			digitalWrite(LED_R, LOW);
			delay(300);
			digitalWrite(LED_R, HIGH);
			delay(300);
			digitalWrite(LED_R, LOW);
			delay(300);
			digitalWrite(LED_R, HIGH);
			delay(300);
			digitalWrite(LED_R, HIGH);
			ledgreen_en = 0;
			
	}
	pthread_exit(nullptr);
}

/**********************************************************************************************//**
 * \fn	void* buzzer_function(void* arg)
 *
 * \brief	Buzzer function
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **************************************************************************************************/

void* buzzer_function(void* arg) {
	while (1) {
		if (1 == buzzer_en)
		{
			softToneCreate(BUZZER);
			softToneWrite(BUZZER, 3000);
			delay(350);
			softToneStop(BUZZER);
			
			buzzer_en = 0;
			
//			digitalWrite(LED_R, LOW);
//			digitalWrite(LED_G, HIGH);
//			delay(2000);
//			digitalWrite(LED_G, LOW);
//			delay(100);
//			digitalWrite(LED_R, HIGH);
		}
		else if (2 == buzzer_en)
		{
			softToneCreate(BUZZER);
			softToneWrite(BUZZER, 3000);
			delay(200);
			softToneStop(BUZZER);
			
			delay(50);
			softToneCreate(BUZZER);
			softToneWrite(BUZZER, 3000);
			delay(200);
			softToneStop(BUZZER);
			
			buzzer_en = 0;
			
//			digitalWrite(LED_G, LOW);
//			digitalWrite(LED_R, LOW);
//			delay(300);
//			digitalWrite(LED_R, HIGH);
//			delay(300);
//			digitalWrite(LED_R, LOW);
//			delay(300);
//			digitalWrite(LED_R, HIGH);
//			delay(300);
//			digitalWrite(LED_R, LOW);
//			delay(300);
//			digitalWrite(LED_R, HIGH);
//			delay(300);
//			digitalWrite(LED_R, LOW);
//			delay(300);
//			digitalWrite(LED_R, HIGH);
//			delay(300);
//			digitalWrite(LED_R, HIGH);
			
		}
	}
}

/**********************************************************************************************//**
 * \fn	void* whitelist_function(void* arg)
 *
 * \brief	Whitelist function
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **************************************************************************************************/

void* whitelist_function(void* arg)
{
	//TODO: Get whitelist 
	printf("whiteList function      \n");
	MemoryStruct white_list;
	uint8_t first_time = 0;
	uint32_t time_interval = 0;
	unsigned int now;
	while (true) {
		if (0 != first_time) {

			sleep(300);
		}
		else {
			first_time++;
		}

		 
		printf("get whitelist\n");
		white_list = get_whitelist((char*)config.sDeviceID.c_str());
		if (200 == white_list.res_code) {
			
			//printf("whitelist \n %s  \n ", white_list.memory);
			std::string white_list_str = std::string(white_list.memory);
			

			//std::cout << white_list_str << std::endl;
		    size_t white_list_code = white_list_str.find("\"success\":true");
			uint16_t white_err_code =  white_list_str.find("\"message\":");
			if (white_list_code ==  std::string::npos)
			{
				char buf[80];
				time_t time_ = time(nullptr);
				struct tm* tm = localtime(&time_);
				
				white_list_str = white_list_str.substr(white_err_code, 42);
				char log_[100];
				sprintf(log_, "%s at %s\n", white_list_str.c_str(),asctime(tm));
				log_reader("white list err", log_);
				continue;
			}
			
			uint32_t white_list_begin = white_list_str.find("\"whiteList\":[");
			uint32_t white_list_end = white_list_str.find_last_of("]}");
//			printf("\nbegin:  %d,end %d , (begin - end) %d \n", white_list_begin, white_list_end, white_list_end - white_list_begin);

			white_list_str = white_list_str.substr(white_list_begin + 14, white_list_end - white_list_begin - 2 - 14);
			int count = 0;
			int index = 0;
			while (0 < (index = white_list_str.find("\",\""))) {
				count++;

				white_list_str.replace(index, 3, "\r\n");

//				index = white_list_str.find("\",\"");
			}

			//std::cout << white_list_str << std::endl;

			uint32_t numOfCard = (white_list_end + 1 - (white_list_begin - 2)) / 22;
			std::cout << R"(numOfCard whitelist  )" << numOfCard << "\n" << std::endl;
			if (numOfCard == 0) {
				remove(R"(/root/kiin/whitelist.txt)");
				continue;
			}
			

			//End write to flash
			//Store White to file
			FILE* p = fopen("/root/kiin/whitelist.txt", "w+");
			if (nullptr == p) {
				printf("open failed with error %d", errno);
			}
			fprintf(p, "%s", white_list_str.c_str());

			fclose(p);
		}
		else {
			time_t time_ = time(nullptr);
			struct tm* tm = localtime(&time_);
	
			char log_[100];
			sprintf(log_, "get whitelist failed at %s: rescode: %d", asctime(tm), white_list.res_code);
			log_reader("whitelist", log_);
			printf("resquest errr: %d\n", white_list.res_code);
		}
	}
	return nullptr;
}

/**********************************************************************************************//**
 * \fn	void initial_hardware(void)
 *
 * \brief	Initial hardware
 *
 * \author	Zeder
 * \date	6/25/2020
 **************************************************************************************************/


void initial_hardware(void)
{
	wiringPiSetup();
	relay_init();
	lcd_init();
	led_init();
	fd = 0;
	//	int spi_error = Rc522_spiInit(&Rc522_fd);
	//	InitRc522();
}

/**********************************************************************************************//**
 * \fn	void read_card()
 *
 * \brief	Reads the card
 *
 * \author	Zeder
 * \date	6/25/2020
 **************************************************************************************************/
byte* data;
byte datalen;
void read_card() {
	
	std::string cardNumber;
	
	if(!mfrc.FastDetect())
		return;
 	if (!mfrc.PICC_ReadCardSerial())
		return;
	switch (mfrc.PICC_GetType(mfrc.uid.sak))
	{
		case MFRC522::PICC_TYPE_MIFARE_1K:
			cardNumber = ClassicService();
			break;
		case MFRC522::PICC_TYPE_ISO_14443_4:
			cardNumber = MifareService();
			break;
	}
	
	///*Read name*/
//	aids[0].data[0] = 0x03;
//	aids[0].data[1] = 0x00;
//	aids[0].data[2] = 0xDF;
//	
//	response = mfrc.MIFARE_DESFIRE_SelectApplication(&tag, aids);
//	if (!mfrc.IsStatusCodeOK(response)) {
//		printf("Failed to select application IDs! \n");
//		return;
//	}
//	byte name_content[80];
//	size_t fileNameLength = 60;
//
//	response = mfrc.MIFARE_DESFIRE_ReadData(&tag, 0x01, 6, fileNameLength, name_content, &fileNameLength);
//
//	
//	if (!mfrc.IsStatusCodeOK(response)) {
//		printf("Failed to read name! \n");
//		return;
//	}
//	
//	std::cout << "name  " << name_content << std::endl;
//	std::string name_str = std::string((char*)name_content);
//	name_str = name_str.substr(0, name_str.size());
//	std::wstring str_turned_to_wstr = std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(name_str);
//	str_turned_to_wstr = RemoveVietnameseTone(str_turned_to_wstr);
//
//	name_str = std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(str_turned_to_wstr);
//	
//	std::cout << name_str << std::endl;
	
	byte check = check_in_whitelist((char*)cardNumber.c_str());
	check = 0;
	
	char buf[80];
	struct tm* tm;
	
	get_time(&tm);
	
	sprintf(buf, "%04d%02d%02d%02d%02d%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	
	if (0 == check)
	{
		sprintf(tmp_card, "%02X", fileContent[i]);
		tmp_card_str.append(tmp_card);
		
	}
	// Store Card in log attendance
	tmp_card_str = tmp_card_str.substr(1, tmp_card_str.length());
	
	std::cout << tmp_card_str << std::endl;
	
	char* card__ = (char*)tmp_card_str.c_str();
	byte check;
//	check = check_in_whitelist((char*)tmp_card_str.c_str());
	check = 0;
	if (0 == check)
	{
//		ClrLcd();
//	    lcdLoc(LINE1);
//		typeln("Xin Chao");
//		lcdLoc(LINE2);
//		typeln(name_str.c_str());
		
//		digitalWrite(RELAY, HIGH);
//		delay(100);
//		digitalWrite(RELAY, LOW);
//		auto asyncLazy = std::async(std::launch::async,
//			[]{ 
//				buzzer_control(0); 
//			});
		std::raise(SIGUSR1);
		usbFd = serialOpen(usbDev0, 38400);
		serialPuts(usbFd, OP);
		serialClose(usbFd);
//		pthread_kill(thread_buzzer, 9);
//		pthread_cancel(thread_buzzer);
		buzzer_en = 1;

//		thread_buzzer_id = pthread_create(&thread_buzzer, nullptr, buzzer_function, nullptr);
//		pthread_join(thread_buzzer);
		
		ledgreen_en = 1;
		while (!(0 == log_attendance_bs)); //wait not busy
		log_attendance_bs = 1;
		FILE* log_attendance = fopen("/root/kiin/logattendance.txt", "a+"); 
		if (nullptr == log_attendance)
		{
			log_attendance_bs = 0;
			printf("failed to open logfile \n");
			return;
		}
		
		std::cout << "size " << tmp_card_str.size() << std::endl;
		fprintf(log_attendance, "%s,%s\n", tmp_card_str.c_str(),buf);
		
		fclose(log_attendance);
		
		
		log_attendance_bs = 0;
		printf("attendance success \n");
	}
	else
	{
		
		buzzer_en = 2;
		ledred_en = 2;
		
		
//		thread_buzzer_id = pthread_create(&thread_buzzer, nullptr, buzzer_function, nullptr);

		char log_[100];
		sprintf(log_, "%s not in whitelist at %s", tmp_card_str.c_str(),asctime(tm));
		log_reader("mfrc", log_);
		printf("not in whitelist \n");
	}
	
	
	
}


/**********************************************************************************************//**
 * \fn	byte check_in_whitelist(char* card_number)
 *
 * \brief	Check in white list
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param [in,out]	card_number	If non-null, the card number.
 *
 * \returns	A byte.
 **************************************************************************************************/
void add_attendance_list(const char* cardnumber)
{
	char buf[80];
	struct tm* tm;
	
	get_time(&tm);
	
	sprintf(buf, "%04d%02d%02d%02d%02d%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	while (!(0 == log_attendance_bs)) ; //wait not busy
	log_attendance_bs = 1;
	FILE* log_attendance = fopen("/root/kiin/logattendance.txt", "a+"); 
	if (nullptr == log_attendance)
	{
		log_attendance_bs = 0;
		printf("failed to open logfile \n");
		return;
	}
	//		
	std::cout << "size " << cardnumber << std::endl;
	fprintf(log_attendance, "%s,%s\n", cardnumber, buf);
	//		
	fclose(log_attendance);
	log_attendance_bs = 0;
}
byte check_in_whitelist(char* card_number) {
	
	printf("check in white list \n");
	byte result;
	FILE* ptr_w; 
	char* file_buffer;
	long numbytes;
	const char* cp_whitelist = "cp /root/kiin/whitelist.txt /root/kiin/whitelist_clone.txt";

	system((const char*)cp_whitelist);


	//std::string card_num_str = std::string 
	ptr_w = fopen("/root/kiin/whitelist_clone.txt", "r");
	if (ptr_w == nullptr) {
		return 1;
	}
	fseek(ptr_w, 0L, SEEK_END);
	numbytes = ftell(ptr_w);

	/* reset the file position indicator to
	the beginning of the file */
	fseek(ptr_w, 0L, SEEK_SET);

	/* grab sufficient memory for the
	buffer to hold the text */
	file_buffer = (char*)calloc(numbytes, sizeof(char));

	/* memory error */
	if (file_buffer == nullptr)
		return 1;

	/* copy all the text into the buffer */
	fread(file_buffer, sizeof(char), numbytes, ptr_w);
	fclose(ptr_w);

	const char* rm = "rm /root/kiin/whitelist_clone.txt";
	system(rm);
	/* confirm we have read the file by
	outputting it to the console */
	//printf("The file called test.dat contains this text\n\n%s \n", file_buffer);
	std::string file_buffer_str = std::string(file_buffer);
//	std::cout << "card check " << card_number << std::endl;
	std::size_t found = file_buffer_str.find(std::string(card_number));
	if (found != std::string::npos) {
		result = 0; //found
	}
	else result = 1; // not found;
	/* free the memory we used for the buffer */
	free(file_buffer);
	file_buffer_str.clear();

	return result;
}

/**********************************************************************************************//**
 * \fn	void* keep_alive(void* arg)
 *
 * \brief	Keep alive
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **************************************************************************************************/

void* keep_alive(void* arg)
{
	uint32_t time_interval = 0;
	unsigned int now;
	uint8_t res = 0;
	printf("keep alive \n");
	for (;;)
	{
		//		time_interval = micros();
		//		while (!(TIME_INTERVAL_KEEPALIVE < (micros() - time_interval) / 1000)) {
		//			
		//		}
		//std::count << config.sDeviceID << std::endl;
		res = device_alive((char*)config.sDeviceID.c_str());
		if (200 == res)
		{		
			printf("keep alive \n");
		}
		else
		{
			char log_[100];
			time_t time_ = time(nullptr);
			struct tm* tm = localtime(&time_);
			sprintf(log_, "keep alive fail with rescode %d at %s",res,asctime(tm));
			log_reader("keep alive:", log_);
		}
		sleep(60);
	}
}

/**********************************************************************************************//**
 * \fn	void* post_attendace_function(void* arg)
 *
 * \brief	Posts an attendance function
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **************************************************************************************************/

void* post_attendace_function(void* arg)
{
	printf("post attendance \n");
	byte result;

	char* log_file_buffer;
	long numbytes;
	long num_card;
	uint32_t time_interval = 0;
	unsigned int now;
	char log_[100];
	time_t time_ = time(nullptr);
	
	const char* log_atten_path = "/root/kiin/logattendance.txt";
	const char* log_atten_temp = "/root/kiin/logattendance_.txt";
	for (;;)
	{
		std::cout << "post att" << std::endl; 
		sleep(10);
//		time_interval = micros();
//		while (!(TIME_INTERVAL_POSTATTENDANCE < (micros() - time_interval) / 1000)) {
//			
//		}
		copy_contents_file(log_atten_path, log_atten_temp);
//		const char* cp_log = "cp /root/kiin/logattendance.txt /root/kiin/logattendance_.txt";
//		system(cp_log);
		remove(log_atten_path);
		
		
		std::fstream log_temp(log_atten_temp);
		if (!log_temp.is_open())
		{
			std::cout << "File could not be opened " << std::endl;
			continue;
		}
		
		log_temp.seekg(0, std::ios_base::end);		
		auto length = log_temp.tellg();
		log_temp.seekg(0, std::ios_base::beg);
		
		if (0 == length)
		{
			continue;
		}
		
		
		length = length / 35;
		std::cout << "length  " << length << std::endl;
		
		auto max_for = length / 20;
		std::cout << "max for " << max_for << std::endl;
		
		auto line = std::string { };
		auto body = std::string { };
		auto line_for_save = std::string {};
		
		
		if (1 == length)
		{
			std::cout << "1 card for att" << std::endl;
			std::getline(log_temp, line);
			body.append("{\"deviceId\":\"");
			body.append(config.sDeviceID);
			body.append("\",\"cardNumber\":\"");
			body.append(line.substr(0,19));
			body.append("\",\"dateTime\":\"");
			body.append(line.substr(20,14));
			body.append("\"}");
			
			auto rescode = post_attendance((char*)body.c_str());
			body.clear();
			log_temp.close();
			
			if (200 == rescode)
			{
				std::cout << "POST OK ! " << std::endl;
				
				remove(log_atten_temp);
			}
			else {
				printf("post failed\n");
				while (!(0 == log_attendance_bs)) ;
				log_attendance_bs = 1;
				FILE* ptr = fopen(log_atten_path, "a");
				if (nullptr == ptr)
				{
					//	
					FILE* ptr = fopen(log_atten_path, "a+");
				}
				fprintf(ptr, "%s\n", line.c_str());
				fclose(ptr);
				log_attendance_bs = 0;
//				//			
//				char log_[100];
//				time_t time_ = time(nullptr);
//				struct tm* tm = localtime(&time_);
//				sprintf(log_, "post attendance failed with rescode %d at %s", rescode, asctime(tm));
//				log_reader("post attendance", log_);
			}
			continue;
		}
		
		
		uint8_t line_remove = 0;
		for (auto j  = 0; j < length; j++)
		{				
			std::getline(log_temp, line);
			if (!line.empty())
			{
				line_remove++;
				line_for_save.append(line);
				line_for_save.append("\n");
				auto tmp_card = line.substr(0, 19);
				auto time =  line.substr(20, 14);
				body.append("{\"deviceId\":\"");
				body.append(config.sDeviceID);
				body.append("\",\"cardNumber\":\"");
				body.append(tmp_card);
				body.append("\",\"dateTime\":\"");
				body.append(time);
				if (((line_remove == 10) || (j == ((int)length - 1))) && (j != 0))
				{
					body.append("\"}");
					std::cout << body << std::endl;
					auto rescode = post_attendance((char*)body.c_str());
					body.clear();
					if (200 == rescode)
					{
						std::cout << "POST OK ! " << std::endl;
						log_temp.close();
						printf("line remove: %d \n", line_remove);
						delete_line(log_atten_temp, line_remove);
						line_remove = 0;
						log_temp.open(log_atten_temp);
						
						
					}
					else {
						printf("post failed\n");
				
						while (!(0 == log_attendance_bs)) ;
				
						log_attendance_bs = 1;
						FILE* ptr = fopen(log_atten_path, "a");
						if (nullptr == ptr)
						{
							FILE* ptr = fopen(log_atten_path, "a+");
//							continue;
						}
						std::cout << line_for_save.size() << std::endl;
						std::cout << "line for save: " << std::endl << line_for_save;
						fprintf(ptr, "%s", line_for_save.c_str());
						fclose(ptr);
						log_attendance_bs = 0;
						//			
						
						struct tm* tm = localtime(&time_);
						sprintf(log_, "post attendance failed with rescode %d at %s", rescode, asctime(tm));
//						log_reader("post attendance", log_);
						
						log_temp.close();
						printf("line remove: %d \n", line_remove);
						delete_line(log_atten_temp, line_remove);
						line_remove = 0;
						log_temp.open(log_atten_temp);
					}	
					line_for_save.clear();
				}
				else {
					body.append("\"},");
				}
			}
		}
		remove(log_atten_temp);
		

	}
}

/**********************************************************************************************//**
 * \fn	void delete_line(const char *file_name, int n)
 *
 * \brief	Deletes the line
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param 	file_name	Filename of the file.
 * \param 	n		 	An int to process.
 **************************************************************************************************/
void delete_line(const char *file_name, int n) 
{ 
	// open file in read mode or in mode 
	std::ifstream is(file_name); 
  
	// open file in write mode or out mode 
	std::ofstream ofs; 
	ofs.open("/root/kiin/temp.txt", std::ofstream::out); 
	
	// loop getting single characters 
	char c; 
	int line_no = 0; 
	auto line = std::string {};
	while (std::getline(is,line)) 
	{ 
		// if a newline character 
		if(!line.empty())
		{
			line_no++; 
			if (line_no >= n + 1) 
				ofs << line << std::endl; 
		}
		// file content not to be deleted	
	} 
  
	// closing output file 
	ofs.close(); 
  
	// closing input file 
	is.close(); 
  
	// remove the original file 
	remove(file_name); 
  
	// rename the file 
	rename("/root/kiin/temp.txt", file_name);
} 

/**********************************************************************************************//**
 * \fn	void copy_contents_file(const char *source, const char *dest)
 *
 * \brief	Copies the contents file
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param 	source	Another instance to copy.
 * \param 	dest  	Destination for the.
 **************************************************************************************************/

void copy_contents_file(const char *source, const char *dest) 
{ 
	// open file in read mode or in mode 
	std::ifstream is(source); 
  
	// open file in write mode or out mode 
	std::ofstream ofs; 
	ofs.open(dest, std::ofstream::app); 

	auto line = std::string { };
	while (std::getline(is, line)) 
	{ 
		// if a newline character 
		if(!line.empty())
		{
		    ofs << line << std::endl; 
		}
		// file content not to be deleted	
	} 
  
	// closing output file 
	ofs.close(); 
	// closing input file 
	is.close(); 
 
}

/**********************************************************************************************//**
 * \fn	std::wstring RemoveVietnameseTone(const std::wstring& text)
 *
 * \brief	Removes the vietnamese tone described by text
 *
 * \author	Zeder
 * \date	6/29/2020
 *
 * \param 	text	The text.
 *
 * \returns	A std::wstring.
 **************************************************************************************************/

std::wstring RemoveVietnameseTone(const std::wstring& text) {
	std::wstring result(text);
	result = std::regex_replace(result, std::wregex(L"à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|/g"), L"a");
	result = std::regex_replace(result, std::wregex(L"À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|/g"), L"A");
	result = std::regex_replace(result, std::wregex(L"è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|/g"), L"e");
	result = std::regex_replace(result, std::wregex(L"È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|/g"), L"E");
	result = std::regex_replace(result, std::wregex(L"ì|í|ị|ỉ|ĩ|/g"), L"i");
	result = std::regex_replace(result, std::wregex(L"Ì|Í|Ị|Ỉ|Ĩ|/g"), L"I");
	result = std::regex_replace(result, std::wregex(L"ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|/g"), L"o");
	result = std::regex_replace(result, std::wregex(L"Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|/g"), L"O");
	result = std::regex_replace(result, std::wregex(L"ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|/g"), L"u");
	result = std::regex_replace(result, std::wregex(L"Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|/g"), L"U");
	result = std::regex_replace(result, std::wregex(L"ỳ|ý|ỵ|ỷ|ỹ|/g"), L"y");
	result = std::regex_replace(result, std::wregex(L"Ỳ|Ý|Ỵ|Ỷ|Ỹ|/g"), L"y");
	result = std::regex_replace(result, std::wregex(L"đ"), L"d");
	result = std::regex_replace(result, std::wregex(L"Đ"), L"D");
	return result;
}

/**********************************************************************************************//**
 * \fn	void GPIO_OpenGate(void)
 *
 * \brief	Gpio open gate
 *
 * \author	Zeder
 * \date	7/10/2020
 **************************************************************************************************/

void GPIO_OpenGate()
{
	int usbFd;
	usbFd = serialOpen(usbDev0, 38400);
	serialPuts(usbFd, OP);
	serialClose(usbFd);
}
