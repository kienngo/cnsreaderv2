#include "K3Machine.h"

#include <utility>

K3Machine* K3Machine::K3Machine_{nullptr};
std::mutex K3Machine::mutex_;
K3Machine::K3Machine(const std::string port)
	: K3MachinePort(port)
{
	
}

K3Machine* K3Machine::getInstance(const std::string& port)
{
	std::lock_guard<std::mutex> lock(mutex_);
	if (K3Machine_ == nullptr)
	{
		K3Machine_ = new K3Machine(port);
	}
	return K3Machine_;
}

K3Machine::~K3Machine()
{
	close();
}

bool K3Machine::open()
{
	//TODO:
		
	fd = serialOpen(K3MachinePort.c_str(), 115200);
	if (fd < 0)
	{
		fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
		return false;
	}
	return true;
}

bool K3Machine::close()
{
	//Serial
    serialClose(fd);
	return true;
}

auto K3Machine::write(char* data) const -> void
{
	serialPuts(fd, data);
}

auto K3Machine::read() -> int
{
	//TODO: Code read data from ttyUSB0
   return	serialGetchar(fd);
}

auto K3Machine::data_avails() -> int
{
	return serialDataAvail(fd);
}
