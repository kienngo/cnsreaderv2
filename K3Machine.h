#ifndef SERIAL_H__
#define SERIAL_H__

#include <stdio.h>
#include <string.h>
#include <wiringPi.h>
#include <errno.h>
#include <mutex>
#include <string>
#include <wiringSerial.h>

enum K3MeasureStatus {
	TEMP_MEASURED, TEMP_NOT_MEASURED
};

enum K3TempSafety
{
	NO_WARNING = 0,   //Khong thuc hien canh bao va cho tag the binh thuong
	HIGH_SAFETY		//Canh bao va cho tag the
};
 enum K3TempFormat
{
	CELSIUS,
	FAHRENHEIT
};
 enum K3WordMode
{
	SURFACE,
	BODY,
	COUNT
};
 enum K3OutPutType
{
	SWITH_MODE,
	PULSE_MODE
};

 struct K3Proconfig
{
	uint8_t TempFormat;
	uint8_t mode;
	uint8_t output;
	uint8_t backlight;
	uint8_t TempSafetyLevel;
	float AlarmUpperLimit;
	bool bBodyTempTaken;
	std::string k3_msg_access, k3_msg_remind, k3_msg_reject;

	unsigned int default_timeout;
	unsigned int remind_timeout;
	unsigned int reject_timeout;
	//Software
	K3TempSafety k3_TempSafety;
	K3MeasureStatus k3_measure_status;
} ;
class K3Machine
{

public:
		
	K3Machine(K3Machine &other) = delete;
	K3Proconfig	m_K3Config;
	void operator=(const K3Machine&) = delete;
	static K3Machine *getInstance(const std::string& port);
	bool open();
	bool close();
	auto write(char* data) const -> void;
	auto read() -> int;

	auto data_avails()-> int;
private:
	static K3Machine * pinstance_;
	static std::mutex mutex_;
	int fd;	
protected:
	K3Machine(const std::string port);
	~K3Machine();
	static K3Machine* K3Machine_;
	std::string K3MachinePort;
};
#endif

